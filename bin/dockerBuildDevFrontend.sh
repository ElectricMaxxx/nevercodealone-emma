#!/bin/bash

export TAG=$(git describe --abbrev=0 --tags)
export VERSION_TAG=${TAG};

docker pull $DOCKER_IMAGE/node-builder-dev:latest
docker build ./nevercodealone --target node-builder-dev -t $DOCKER_IMAGE/node-builder-dev:latest \
  --cache-from $DOCKER_IMAGE/node-builder-dev:latest
#docker push $DOCKER_IMAGE/node-builder-dev:latest

docker pull $DOCKER_IMAGE/frontend-development:$TAG
docker build  ./nevercodealone --target frontend-development -t $DOCKER_IMAGE/frontend-development:$TAG \
  --cache-from $DOCKER_IMAGE/node-builder-dev:latest \
  --cache-from $DOCKER_IMAGE/frontend-development:$TAG
#docker push $DOCKER_IMAGE/frontend-development:$TAG