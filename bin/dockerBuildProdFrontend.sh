#!/bin/bash

export TAG=$(git describe --abbrev=0 --tags)
export VERSION_TAG=${TAG};


#docker pull $DOCKER_IMAGE/node-builder-prod:latest
docker build ./nevercodealone --target node-builder-prod -t $DOCKER_IMAGE/node-builder-prod:latest \
  --cache-from $DOCKER_IMAGE/node-builder-prod:latest
#docker push $DOCKER_IMAGE/node-builder-prod:latest

#docker pull $DOCKER_IMAGE/frontend-production:$TAG
docker build  ./nevercodealone --target frontend-production -t $DOCKER_IMAGE/frontend-production:$TAG \
  --cache-from $DOCKER_IMAGE/node-builder-prod:latest \
  --cache-from $DOCKER_IMAGE/frontend-production:$TAG
#docker push $DOCKER_IMAGE/frontend-production:$TAG
