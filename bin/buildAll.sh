#!/bin/bash

export TAG=$(git describe --abbrev=0 --tags)
export VERSION_TAG=${TAG};
#docker pull $DOCKER_IMAGE/node-builder-dev:latest
#docker pull $DOCKER_IMAGE/node-builder-prod:latest
#docker pull $DOCKER_IMAGE/node-dev-base:latest
#docker pull $DOCKER_IMAGE/frontend-production:$TAG
#docker pull $DOCKER_IMAGE/frontend-development:$TAG

docker build ./nevercodealone --target node-builder-dev -t $DOCKER_IMAGE/node-builder-dev:latest \
  --cache-from $DOCKER_IMAGE/node-builder-dev:latest

docker build ./nevercodealone --target node-builder-prod -t $DOCKER_IMAGE/node-builder-prod:latest \
  --cache-from $DOCKER_IMAGE/node-builder-prod:latest

docker build ./nevercodealone --target node-dev-base -t $DOCKER_IMAGE/node-dev-base:latest \
  --cache-from $DOCKER_IMAGE/node-builder:latest \
    --cache-from $DOCKER_IMAGE/node-dev-base:latest

docker build  ./nevercodealone --target frontend-production -t $DOCKER_IMAGE/frontend-production:$TAG \
--cache-from $DOCKER_IMAGE/node-builder-prod:latest \
--cache-from $DOCKER_IMAGE/frontend-production:$TAG

docker build  ./nevercodealone --target frontend-development -t $DOCKER_IMAGE/frontend-development:$TAG \
  --cache-from $DOCKER_IMAGE/node-builder-dev:latest \
  --cache-from $DOCKER_IMAGE/node-dev-base:latest \
  --cache-from $DOCKER_IMAGE/frontend-development:$TAG

#docker push $DOCKER_IMAGE/node-builder-dev:latest
#docker push $DOCKER_IMAGE/node-builder-prod:latest
#docker push $DOCKER_IMAGE/node-dev-base:latest
#docker push $DOCKER_IMAGE/frontend-production:$TAG
#docker push $DOCKER_IMAGE/frontend-development:$TAG


#docker pull $DOCKER_IMAGE/composer:latest
#docker pull $DOCKER_IMAGE/api-server:latest
#docker pull $DOCKER_IMAGE/api-production:$TAG
#docker pull $DOCKER_IMAGE/api-development:$TAG

docker build ./emma-backend --target composer -t $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/composer:latest

docker build  ./emma-backend --target api-server -t $DOCKER_IMAGE/api-server:latest \
  --cache-from $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/api-server:latest

docker build  ./emma-backend --target api-production -t $DOCKER_IMAGE/api-production:$TAG \
  --cache-from $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/api-server:latest \
  --cache-from $DOCKER_IMAGE/api-production:$TAG

docker build ./emma-backend --target api-development  -t $DOCKER_IMAGE/api-development:$TAG \
  --cache-from $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/api-server:latest \
  --cache-from $DOCKER_IMAGE/api-development:$TAG

#docker push $DOCKER_IMAGE/composer:latest
#docker push $DOCKER_IMAGE/api-server:latest
#docker push $DOCKER_IMAGE/api-production:$TAG
#docker push $DOCKER_IMAGE/api-staging:$TAG
#docker push $DOCKER_IMAGE/api-development:$TAG
