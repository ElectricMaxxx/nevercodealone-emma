#!/bin/bash

export TAG=$(git describe --abbrev=0 --tags)
export VERSION_TAG=${TAG};

#docker pull $DOCKER_IMAGE/composer:latest
docker build ./emma-backend --target composer -t $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/composer:latest
#docker push $DOCKER_IMAGE/composer:latest

#docker pull $DOCKER_IMAGE/api-server:latest
docker build  ./emma-backend --target api-server -t $DOCKER_IMAGE/api-server:latest \
  --cache-from $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/api-server:latest
#docker push $DOCKER_IMAGE/api-server:latest

#docker pull $DOCKER_IMAGE/api-development:$TAG
docker build  ./emma-backend --target api-development -t $DOCKER_IMAGE/api-development:$TAG \
  --cache-from $DOCKER_IMAGE/composer:latest \
  --cache-from $DOCKER_IMAGE/api-server:latest \
  --cache-from $DOCKER_IMAGE/api-development:$TAG
#docker push $DOCKER_IMAGE/api-development:$TAG
