import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { AdminUser, Organisation, Role } from '@nevercodealone/data-models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'nevercodealone-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserEditComponent implements OnInit, OnChanges {
  @Input() user: AdminUser | null = null;
  @Input() organisations: Organisation[] = [];
  @Input() roles: Role[] = [];
  @Output() submitUser: EventEmitter<AdminUser> = new EventEmitter<AdminUser>();
  editUserForm: FormGroup = this.formBuilder.group({
    email: [
      null,
      [
        Validators.required,
        Validators.pattern(
          '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'
        ),
      ],
    ],
    user_name: [null, Validators.required],
    roles: [null, Validators.required],
    first_name: [null, Validators.required],
    last_name: [null, Validators.required],
    organisation: [null, Validators.required],
  });

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(): void {
    this.setUserToForm();
  }

  ngOnInit() {
    this.setUserToForm();
  }

  private setUserToForm(): void {
    this.editUserForm.patchValue({
      email: null !== this.user ? this.user.email : '',
      user_name: null !== this.user ? this.user.user_name : '',
      roles: null !== this.user ? this.user.roles : [],
      first_name: null !== this.user ? this.user.first_name : '',
      last_name: null !== this.user ? this.user.last_name : '',
      organisation: null !== this.user ? this.user.organisation_id : '',
    });
  }
  submit(): void {
    if (this.editUserForm.invalid) {
      return;
    }
    if (null === this.user) {
      this.user = {
        first_name: '',
        last_name: '',
        user_name: '',
        roles: [Role.User],
        email: '',
        organisation_name: '',
      };
    }
    const formValues = this.editUserForm.getRawValue();
    this.user.first_name = formValues.first_name;
    this.user.last_name = formValues.last_name;
    this.user.user_name = formValues.user_name;
    this.user.roles = formValues.roles;
    this.user.email = formValues.email;
    this.user.email = formValues.email;
    const orgs = this.organisations.filter(
      (org: Organisation) => (org.id = formValues.organisation)
    );
    if (orgs.length === 1) {
      this.user.organisation_name = orgs[0].name;
      this.user.organisation_id = orgs[0].id;
    }
    this.submitUser.emit(this.user);
  }

  isInvalid(controlName: string): boolean {
    return (
      this.editUserForm.controls[controlName].touched &&
      this.editUserForm.controls[controlName].invalid
    );
  }

  hasError(controlName: string, errorCode: string): boolean {
    return this.editUserForm.controls[controlName].hasError(errorCode);
  }
}
