import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { AdminUser } from '@nevercodealone/data-models';

@Component({
  selector: 'nevercodealone-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListComponent {
  @Input() users: AdminUser[] | null = [];
  @Output()
  selectUser: EventEmitter<AdminUser | null> = new EventEmitter<AdminUser | null>();
  displayedColumns: string[] = [
    'user_name',
    'role',
    'organisation_name',
    'first_name',
    'last_name',
    'email',
  ];
  @Input() selectedUser: AdminUser | null = null;

  onSelectUser(row: AdminUser): void {
    if (this.selectedUser?.email === row.email) {
      this.selectedUser = null;
      this.selectUser.emit(null);
    } else {
      this.selectedUser = row;
      this.selectUser.emit(row);
    }
  }
}
