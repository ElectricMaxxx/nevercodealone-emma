import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Injectable } from '@angular/core';
import { AdminUser, Organisation } from '@nevercodealone/data-models';
import { BehaviorSubject, Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { UserService } from './user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { OrganisationService } from 'apps/emma/src/app/services';

export interface UsersState {
  entities: { [id: number]: AdminUser };
  loaded: boolean;
  loading: boolean;
  selectedUser: AdminUser | null;
  createNewUser: boolean;
  organisations: Organisation[];
}

@Injectable()
export class UsersStore extends ComponentStore<UsersState> {
  constructor(
    private userService: UserService,
    private organisationService: OrganisationService
  ) {
    super({
      entities: {},
      loaded: false,
      loading: false,
      selectedUser: null,
      createNewUser: false,
      organisations: [],
    });
  }
  readonly loadUsersEffect = this.effect(
    (usersData$: Observable<AdminUser[]>) => {
      return usersData$.pipe(
        concatMap(() =>
          this.userService.loadUsers().pipe(
            tapResponse(
              (users: AdminUser[]) => this.updateUsers(users),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );
  readonly loadOrganisationsEffect = this.effect(
    (organisationsData: Observable<AdminUser[]>) => {
      return organisationsData.pipe(
        concatMap(() =>
          this.organisationService.getAdminOrganisations().pipe(
            tapResponse(
              (organisations: Organisation[]) =>
                this.updateOrganisations(organisations),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );

  readonly saveUserEffect = this.effect((userData$: Observable<AdminUser>) => {
    return userData$.pipe(
      concatMap((user: AdminUser) =>
        this.userService.saveUser(user).pipe(
          tapResponse(
            (user: AdminUser) => this.updateUser(user),
            (error: HttpErrorResponse) => this.logError(error)
          )
        )
      )
    );
  });

  readonly createUserEffect = this.effect(
    (userData$: Observable<AdminUser>) => {
      return userData$.pipe(
        concatMap((user: AdminUser) =>
          this.userService.createUser(user).pipe(
            tapResponse(
              (user: AdminUser) => this.updateUser(user),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );
  readonly updateUser = this.updater((state: UsersState, user: AdminUser) => ({
    ...state,
    entities: { ...state.entities, [user.id ? user.id : 0]: user },
  }));

  readonly updateUsers = this.updater((state, users: AdminUser[]) => ({
    ...state,
    entities: users.reduce(
      (entities: { [id: number]: AdminUser }, user: AdminUser) => {
        return user.id ? { ...entities, [user.id]: user } : { ...entities };
      },
      { ...state.entities }
    ),
    loaded: true,
    loading: false,
  }));

  readonly updateOrganisations = this.updater(
    (state, organisations: Organisation[]) => ({
      ...state,
      organisations,
    })
  );

  readonly setSelectedUser = this.updater(
    (state, selectedUser: AdminUser | null) => ({
      ...state,
      selectedUser,
    })
  );

  readonly setCreateNew = this.updater((state, createNewUser: boolean) => ({
    ...state,
    createNewUser,
  }));

  readonly usersEntities$: Observable<{
    [id: number]: AdminUser;
  }> = this.select((state) => state.entities);

  readonly users$ = this.select((state) =>
    Object.keys(state.entities).map((key) => state.entities[parseInt(key, 10)])
  );
  readonly selectedUser$ = this.select((state) => state.selectedUser);
  readonly createNewUser$ = this.select((state) => state.createNewUser);
  readonly loaded$ = this.select((state) => state.loaded);
  readonly loading$ = this.select((state) => state.loading);
  readonly organisations$ = this.select(
    (state: UsersState) => state.organisations
  );

  readonly vm$ = this.select(
    this.users$,
    this.usersEntities$,
    this.createNewUser$,
    this.selectedUser$,
    this.loaded$,
    this.loading$,
    this.organisations$,
    (
      users,
      userEntities,
      createNewUser,
      selectedUser,
      loaded,
      loading,
      organisations
    ) => ({
      users,
      userEntities,
      createNewUser,
      selectedUser,
      loaded,
      loading,
      organisations,
    }),
    { debounce: true } // 👈 setting this selector to debounce
  );

  logError = (error: any) => {
    console.log(error);
  };

  saveUser(user: AdminUser) {
    if (user.id) {
      this.saveUserEffect(new BehaviorSubject(user));
    } else {
      this.createUserEffect(new BehaviorSubject(user));
    }
  }
}
