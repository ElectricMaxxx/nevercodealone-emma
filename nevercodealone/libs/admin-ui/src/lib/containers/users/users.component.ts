import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { UsersStore } from './users.store';
import { BehaviorSubject, Observable } from 'rxjs';
import { AdminUser } from '@nevercodealone/data-models';

@Component({
  selector: 'nevercodealone-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UsersStore],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersComponent implements OnInit {
  readonly vm$: Observable<{
    users: AdminUser[];
    userEntities: { [id: number]: AdminUser };
    createNewUser: boolean;
    selectedUser: AdminUser | null;
    loaded: boolean;
    loading: boolean;
  }> = this.usersStore.vm$;

  readonly createNewUser$ = this.usersStore.createNewUser$;
  readonly selectedUser$ = this.usersStore.selectedUser$;
  readonly users$ = this.usersStore.users$;
  readonly organisations$ = this.usersStore.organisations$;

  constructor(private readonly usersStore: UsersStore) {}

  ngOnInit() {
    this.usersStore.loadUsersEffect(new BehaviorSubject([]));
    this.usersStore.loadOrganisationsEffect(new BehaviorSubject([]));
  }

  selectUser(user: AdminUser | null): void {
    this.usersStore.setSelectedUser(user);
  }

  saveUser(user: AdminUser): void {
    this.usersStore.saveUser(user);
  }
}
