import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Injectable } from '@angular/core';
import { CategoryResource, Organisation } from '@nevercodealone/data-models';
import { BehaviorSubject, Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { CategoriesService } from './categories.service';
import { HttpErrorResponse } from '@angular/common/http';
import { OrganisationService } from 'apps/emma/src/app/services';

export interface CategoriesState {
  entities: { [id: number]: CategoryResource };
  loaded: boolean;
  loading: boolean;
  selected: CategoryResource | null;
  created: boolean;
  organisations: Organisation[];
}

@Injectable()
export class CategoriesStore extends ComponentStore<CategoriesState> {
  constructor(
    private categoriesService: CategoriesService,
    private organisationService: OrganisationService
  ) {
    super({
      entities: {},
      loaded: false,
      loading: false,
      selected: null,
      created: false,
      organisations: [],
    });
  }

  readonly loadCategoriesEffect = this.effect(
    (categoriesData$: Observable<CategoryResource[]>) => {
      return categoriesData$.pipe(
        concatMap(() =>
          this.categoriesService.loadCategories().pipe(
            tapResponse(
              (categories: CategoryResource[]) => this.updateCategories(categories),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );
  readonly loadOrganisationsEffect = this.effect(
    (organisationsData: Observable<CategoryResource[]>) => {
      return organisationsData.pipe(
        concatMap(() =>
          this.organisationService.getAdminOrganisations().pipe(
            tapResponse(
              (organisations: Organisation[]) =>
                this.updateOrganisations(organisations),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );

  readonly saveCategoriesEffect = this.effect((categoryData$: Observable<CategoryResource>): Observable<CategoryResource> => {
    return categoryData$.pipe(
      concatMap((category: CategoryResource) =>
        this.categoriesService.saveCategories(category).pipe(
          tapResponse(
            (category: CategoryResource) => this.updatCategory(category),
            (error: HttpErrorResponse) => this.logError(error)
          )
        )
      )
    );
  });

  readonly createCategoryEffect = this.effect(
    (categoryData$: Observable<CategoryResource>) => {
      return categoryData$.pipe(
        concatMap((category: CategoryResource) =>
          this.categoriesService.createCategory(category).pipe(
            tapResponse(
              (category: CategoryResource) => this.updatCategory(category),
              (error: HttpErrorResponse) => this.logError(error)
            )
          )
        )
      );
    }
  );
  readonly updatCategory = this.updater((state: CategoriesState, category: CategoryResource): CategoriesState => ({
    ...state,
    entities: { ...state.entities, [category.id ? category.id : 0]: category },
  }));

  readonly updateCategories = this.updater((state, categories: CategoryResource[]) => ({
    ...state,
    entities: categories.reduce(
      (entities: { [id: number]: CategoryResource }, category: CategoryResource) => {
        return category.id ? { ...entities, [category.id]: category } : { ...entities };
      },
      { ...state.entities }
    ),
    loaded: true,
    loading: false,
  }));

  readonly updateOrganisations = this.updater(
    (state, organisations: Organisation[]) => ({
      ...state,
      organisations,
    })
  );

  readonly setSelectedCategory = this.updater(
    (state, selectedCategory: CategoryResource | null) => ({
      ...state,
      selected: selectedCategory,
    })
  );

  readonly setCreateNew = this.updater((state, createNewCategory: boolean) => ({
    ...state,
    created: createNewCategory,
  }));

  readonly categoriesEntities$: Observable<{
    [id: number]: CategoryResource;
  }> = this.select((state) => state.entities);

  readonly categories$ = this.select((state) =>
    Object.keys(state.entities).map((key) => state.entities[parseInt(key, 10)])
  );
  readonly selectedCategory$ = this.select((state) => state.selected);
  readonly createNewCategory$ = this.select((state) => state.created);
  readonly loaded$ = this.select((state) => state.loaded);
  readonly loading$ = this.select((state) => state.loading);
  readonly organisations$ = this.select(
    (state: CategoriesState) => state.organisations
  );

  readonly vm$ = this.select(
    this.categories$,
    this.categoriesEntities$,
    this.createNewCategory$,
    this.selectedCategory$,
    this.loaded$,
    this.loading$,
    this.organisations$,
    (
      list,
      entities,
      createNew,
      selected,
      loaded,
      loading,
      organisations
    ) => ({
      list,
      entities,
      createNew,
      selected,
      loaded,
      loading,
      organisations,
    }),
    { debounce: true } // 👈 setting this selector to debounce
  );

  logError = (error: any) => {
    console.log(error);
  };

  saveCategory(category: CategoryResource) {
    if (category.id) {
      this.saveCategoriesEffect(new BehaviorSubject(category));
    } else {
      this.createCategoryEffect(new BehaviorSubject(category));
    }
  }
}
