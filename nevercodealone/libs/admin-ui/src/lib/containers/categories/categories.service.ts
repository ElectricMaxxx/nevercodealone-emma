import { Inject, Injectable } from '@angular/core';
import { AdminUser } from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  loadUsers(): Observable<AdminUser[]> {
    return this.http.get<AdminUser[]>(`${this.apiUrl}/admin/users`);
  }

  saveUser(user: AdminUser): Observable<AdminUser> {
    return this.http.put<AdminUser>(
      `${this.apiUrl}/admin/users/${user.id}`,
      user
    );
  }
  createUser(user: AdminUser): Observable<AdminUser> {
    return this.http.post<AdminUser>(`${this.apiUrl}/admin/users`, user);
  }
}
