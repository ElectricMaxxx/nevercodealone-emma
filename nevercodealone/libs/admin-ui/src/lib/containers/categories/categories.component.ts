import { CategoryResource } from './../../../../../data-models/src/lib/category.model';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { CategoriesStore } from './categories.store';
import { BehaviorSubject, Observable } from 'rxjs';
import { AdminUser } from '@nevercodealone/data-models';

@Component({
  selector: 'nevercodealone-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [CategoriesStore],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoriesComponent implements OnInit {
  readonly vm$: Observable<{
    list: CategoryResource[];
    entities: { [id: number]: CategoryResource };
    createNew: boolean;
    selected: CategoryResource | null;
    loaded: boolean;
    loading: boolean;
  }> = this.categoriesStore.vm$;

  readonly createNewCategory$ = this.categoriesStore.createNewCategory$;
  readonly selectedCategory$ = this.categoriesStore.selectedCategory$;
  readonly categories$ = this.categoriesStore.categories$;
  readonly organisations$ = this.categoriesStore.organisations$;

  constructor(private readonly categoriesStore: CategoriesStore) {}

  ngOnInit() {
    this.categoriesStore.loadCategoriesEffect(new BehaviorSubject([]));
    this.categoriesStore.loadOrganisationsEffect(new BehaviorSubject([]));
  }

  selectUser(category: CategoryResource | null): void {
    this.categoriesStore.setSelectedCategory(category);
  }

  saveUser(category: CategoryResource): void {
    this.categoriesStore.saveCategory(category);
  }
}
