import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { PhrasesComponent } from './containers/phrases/phrases.component';
import { UsersComponent } from './containers/users/users.component';
import { ProjectsComponent } from './containers/projects/projects.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AuthGuard } from '@nevercodealone/shared-functions';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';

export const adminUiRoutes: Route[] = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'phrases', component: PhrasesComponent },
  { path: 'projects', component: ProjectsComponent },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableModule,
    MatGridListModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
  ],
  declarations: [
    DashboardComponent,
    PhrasesComponent,
    UsersComponent,
    ProjectsComponent,
    UserListComponent,
    UserEditComponent,
  ],
})
export class AdminUiModule {}
