import { AuthenticationService } from './authentication.service.js';

export function appInitializer(authService: AuthenticationService) {
  return () => {
    return new Promise((resolve) => {
      console.log('refresh token on app start up');
      authService
        .refreshToken()
        .subscribe(null, (error) => {
          console.log(error);
        })
        .add(resolve);
    });
  };
}
