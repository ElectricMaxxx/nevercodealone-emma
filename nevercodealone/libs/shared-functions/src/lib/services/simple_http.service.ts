import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { of } from 'rxjs/observable/of';
import { HTTP_API_URL } from '../shared-functions.module';
import { HttpClientService } from './http_client.service';
import { HttpService } from './http.service';

@Injectable()
export class SimpleHttpService implements HttpService {
  constructor(
    private httpClientService: HttpClientService,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  /**
   * Construct a complete url and post request
   * @param service
   * @param method
   * @param request
   * @returns {Observable<any>}
   */
  public post(service: string, method: string, request: any): Observable<any> {
    console.log('apiUrl', this.apiUrl);
    return of({ name: 'test' });
  }

  private paramaterizeJson(data: any) {
    return Object.keys(data)
      .map((k) => {
        let value = data[k];
        if (typeof value === 'object') {
          value = JSON.stringify(value);
        }
        return encodeURIComponent(k) + '=' + encodeURIComponent(value);
      })
      .join('&');
  }
}
