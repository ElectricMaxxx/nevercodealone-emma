import { Inject, Injectable, OnDestroy } from '@angular/core';

import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { map, tap, delay, finalize } from 'rxjs/operators';

import { Role, User } from '@nevercodealone/data-models';
import { HTTP_BACKEND_URL } from '../shared-functions.module';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

interface LoginResponse extends User {
  token: string;
  data: User;
  refresh_token: string;
}

@Injectable({ providedIn: 'root' })
export class AuthenticationService implements OnDestroy {
  private timer: Subscription = new Subscription();
  private _user = new BehaviorSubject<User | null>(null);
  user$: Observable<User | null> = this._user.asObservable();

  constructor(
    private router: Router,
    private http: HttpClient,
    @Inject(HTTP_BACKEND_URL) private backendUrl: string
  ) {
    window.addEventListener('storage', this.storageEventListener.bind(this));
  }

  ngOnDestroy(): void {
    window.removeEventListener('storage', this.storageEventListener.bind(this));
  }

  private storageEventListener(event: StorageEvent) {
    if (event.storageArea === localStorage) {
      if (event.key === 'logout-event') {
        this.stopTokenTimer();
        this._user.next(null);
      }
      if (event.key === 'login-event') {
        this.startTokenTimer();
      }
    }
  }

  login(username: string, password: string): Observable<LoginResponse> {
    return this.http
      .post<LoginResponse>(`${this.backendUrl}/login_check`, {
        username,
        password,
      })
      .pipe(
        map((response: LoginResponse) => {
          // login successful if there's a jwt token in the response
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.setLocalStorage(response);
          this._user.next({ ...response.data, token: response.token });
          this.startTokenTimer();

          return response;
        })
      );
  }

  logout() {
    this.clearLocalStorage();
    this._user.next(null);
    this.stopTokenTimer();
    this.router.navigate(['login']);
  }

  refreshToken() {
    const refreshToken = localStorage.getItem('refresh_token');
    if (!refreshToken) {
      this.clearLocalStorage();
      return of(null);
    }

    return this.http
      .post<LoginResponse>(`${this.backendUrl}/api/refresh-token`, {
        refresh_token: refreshToken,
      })
      .pipe(
        map((response: LoginResponse) => {
          // login successful if there's a jwt token in the response
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.setLocalStorage(response);
          this._user.next({ ...response.data, token: response.token });
          this.startTokenTimer();

          return response;
        })
      );
  }

  get currentUserValue(): User | null {
    return this._user.value;
  }
  private setLocalStorage(x: LoginResponse): void {
    localStorage.setItem('access_token', x.token);
    localStorage.setItem('refresh_token', x.refresh_token);
    localStorage.setItem('login-event', 'login' + Math.random());
  }

  private getTokenRemainingTime() {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      return 0;
    }
    const jwtToken = JSON.parse(atob(accessToken.split('.')[1]));
    const expires = new Date(jwtToken.exp * 1000);
    return expires.getTime() - Date.now();
  }

  private startTokenTimer() {
    const timeout = this.getTokenRemainingTime();
    this.timer = of(true)
      .pipe(
        delay(timeout),
        tap(() =>
          this.refreshToken().subscribe(null, (error) => {
            console.log(error);
          })
        )
      )
      .subscribe();
  }

  private stopTokenTimer() {
    this.timer?.unsubscribe();
  }

  clearLocalStorage() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('logout-event');
    localStorage.removeItem('login-event');
  }
}
