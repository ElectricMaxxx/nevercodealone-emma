import { Injectable } from '@angular/core';
import { AuthenticationService } from '@nevercodealone/shared-functions';
import { Role, User } from '@nevercodealone/data-models';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationService {
  currentUserValue: User | null = null;
  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.user$.subscribe(
      (user) => (this.currentUserValue = user)
    );
  }

  get isAdmin(): boolean {
    const user = this.currentUserValue;
    if (!user) {
      return false;
    }
    return (
      user.roles.includes(Role.OrganisationAdmin) ||
      user.roles.includes(Role.EmmaAdmin)
    );
  }

  get isOrganisationAdmin(): boolean {
    const user = this.currentUserValue;
    if (!user) {
      return false;
    }
    return user.roles.includes(Role.OrganisationAdmin);
  }

  get isEmmaAdmin(): boolean {
    const user = this.currentUserValue;
    if (!user) {
      return false;
    }
    return user.roles.includes(Role.EmmaAdmin);
  }

  get isUser(): boolean {
    const user = this.currentUserValue;
    if (!user) {
      return false;
    }
    return user.roles.includes(Role.User);
  }
}
