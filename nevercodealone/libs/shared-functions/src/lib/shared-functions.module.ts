import {
  APP_INITIALIZER,
  InjectionToken,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { appInitializer } from './services/app-initializer';
import { AuthenticationService } from './services/authentication.service';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { UnauthorizedInterceptor } from './interceptors/unauthorized.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';

@NgModule({
  imports: [CommonModule],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      multi: true,
      deps: [AuthenticationService],
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
})
export class SharedFunctionsModule {
  constructor(@Optional() @SkipSelf() core: SharedFunctionsModule) {
    if (core) {
      throw new Error(
        'SharedFunctionsModule Module can only be imported to AppModule.'
      );
    }
  }
}

export * from './services/authentication.service';
export * from './services/authorization.service';
export * from './services/simple_http.service';
export * from './guards/auth.guard';

export const HTTP_SERVICE = new InjectionToken('http-service');
export const HTTP_API_URL = new InjectionToken('http-api-url');
export const HTTP_BACKEND_URL = new InjectionToken('http-backend-url');
export const ENV_MODE_PRODUCTION = new InjectionToken('env-mode-production');
