import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
export { CategoryResource, NewCategoryResource } from './category.model';
export { PhraseResource, NewPhraseResource } from './phrase.model';
export { Translation } from './translation.model';
export { Country } from './country.model';
export { Organisation } from './organisation.model';
export { User, Role, AdminUser } from './user.model';

@NgModule({
  imports: [CommonModule],
})
export class DataModelsModule {}
