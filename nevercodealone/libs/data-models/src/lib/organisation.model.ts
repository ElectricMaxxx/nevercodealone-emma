import { PhraseResource } from './phrase.model';
import { Country } from './country.model';
import { User } from './user.model';
import { CategoryResource } from './category.model';

export interface Organisation {
  id: number;
  name: string;
  categories: CategoryResource[];
  phrases: PhraseResource[];
  countries: Country[];
  user: User;
}
