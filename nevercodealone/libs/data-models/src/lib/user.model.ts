export interface User {
  id?: number;
  user_name: string;
  roles: Role[];
  first_name?: string;
  last_name?: string;
  email?: string;
  organisation_name?: string;
  organisation_id?: number;
  token?: string;
}

export interface AdminUser {
  id?: number;
  user_name: string;
  roles: Role[];
  first_name: string;
  last_name: string;
  email: string;
  organisation_name?: string;
  organisation_id?: number;
  token?: string;
}
export enum Role {
  User = 'ROLE_USER',
  OrganisationAdmin = 'ROLE_ORGANISATION_ADMIN',
  EmmaAdmin = 'ROLE_ADMIN',
}
