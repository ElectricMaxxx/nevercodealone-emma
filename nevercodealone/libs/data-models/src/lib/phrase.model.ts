import { CategoryResource } from './category.model';
import { Translation } from './translation.model';

export interface PhraseResource {
  id: number;
  name: string;
  category: CategoryResource;
  translations: Translation[];
}

export interface NewPhraseResource {
  name: string;
  category: CategoryResource;
}
