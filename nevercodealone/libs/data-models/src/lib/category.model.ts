export interface CategoryResource {
  id: number;
  category: string;
  description?: string;
}
export interface NewCategoryResource {
  category: string;
  description?: string;
}
