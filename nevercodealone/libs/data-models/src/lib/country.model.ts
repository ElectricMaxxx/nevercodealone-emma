export interface Country {
  name: string;
  language_key: string;
}
