export interface Translation {
    id?: number;
    language_key: string;
    translation: string;
}
