import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PhraseResource, Country } from '@nevercodealone/data-models';
import { ChatComponent } from './chat.component';
import { Store } from '@ngrx/store';
import { ChatMessageBoardComponent } from './chat-message-board.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CountryService } from '../../services';
import { of, Observable } from 'rxjs';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;
  let countryService: CountryService;
  const StoreStub = {
    select() {
      return {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        subscribe() {},
      };
    },
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    dispatch() {},
  };

  const TestPhrase: PhraseResource = {
    name: 'test phrase',
    id: 1,
    translations: [{ translation: 'test-translation', language_key: 'gb' }],
    category: { id: 1, category: 'test category' },
  };
  beforeEach(
    waitForAsync(() => {
      countryService = jasmine.createSpyObj('CountryService', ['getList']);
      countryService.getList = jasmine.createSpy().and.callFake(
        (): Observable<Country[]> => {
          return of([{ id: 1, name: 'Deutschland', language_key: 'de' }]);
        }
      );

      TestBed.configureTestingModule({
        declarations: [ChatComponent, ChatMessageBoardComponent],
        providers: [
          { provide: Store, useValue: StoreStub },
          { provide: CountryService, useValue: countryService },
        ],
        imports: [RouterTestingModule.withRoutes([])],
      }).compileComponents();
    })
  );
  describe('Base tests', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ChatComponent);
      component = fixture.componentInstance;
      component.selectedCountry = {
        name: 'England',
        language_key: 'gb',
      };
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Phrase selection in one chat board', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ChatComponent);
      component = fixture.componentInstance;
      component.selectedCountry = {
        name: 'England',
        language_key: 'gb',
      };
      fixture.detectChanges();
    });

    it('should create messages on phrase selection', () => {
      component.onSelectRightPhrase(TestPhrase);
      expect(component.leftMessages).toEqual(['test-translation']);
      expect(component.rightMessages).toEqual(['test phrase']);
    });
  });
});
