import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import {
  Country,
  PhraseResource,
  NewPhraseResource,
} from '@nevercodealone/data-models';
import * as fromStore from '../../store';
import { Store } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Utils } from '../../services';
import { CreateNewPhraseAction } from '../../store';
import { ChatMessageModel } from '../../models';

@Component({
  selector: 'nevercodealone-chat',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <nevercodealone-chat-message-board
      [phrases]="(phrases$ | async) ?? []"
      [messages]="leftMessages"
      [country]="selectedCountry$ | async"
      [category]="selectedCategory$ | async"
      (selectPhrase)="onSelectLeftPhrase($event)"
      (createNewPhrase)="onCreateNewPhrase($event)"
    ></nevercodealone-chat-message-board>
    <nevercodealone-chat-message-board
      class="reverse"
      [phrases]="(phrases$ | async) ?? []"
      [messages]="rightMessages"
      [country]="baseCountry"
      [category]="selectedCategory$ | async"
      [isRight]="true"
      (selectPhrase)="onSelectRightPhrase($event)"
      (createNewPhrase)="onCreateNewPhrase($event)"
    ></nevercodealone-chat-message-board>
  `,
})
export class ChatComponent implements OnInit {
  allCountries$ = this.store.select(fromStore.getAllCountries);
  ownCountries$ = this.store.select(fromStore.getCountries);
  phrases$ = this.store.select(fromStore.getAllPhrases);
  selectedCountry$ = this.store.select(fromStore.getSelectedCountry);
  selectedCategory$ = this.store.select(fromStore.getSelectedCategory);
  categories$ = this.store.select(fromStore.getAllCategories);

  selectedCountry: Country | null = null;
  baseCountry: Country = environment.baseCountry;
  leftMessages: ChatMessageModel[] = [];
  rightMessages: ChatMessageModel[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromStore.AppState>
  ) {}

  ngOnInit(): void {
    let categoryId: number | null = null;
    let countryLanguageKey: string | null = null;
    if (this.route.snapshot.paramMap) {
      const categoryIdParameter = this.route.snapshot.paramMap.get(
        'categoryId'
      );
      categoryId =
        categoryIdParameter !== null ? parseInt(categoryIdParameter, 10) : null;
      countryLanguageKey = this.route.snapshot.paramMap.get(
        'countryLanguageKey'
      );
    }
    if (null === categoryId || typeof categoryId === 'undefined') {
      this.router.navigate(['category']);
    }
    if (
      null === countryLanguageKey ||
      typeof countryLanguageKey === 'undefined'
    ) {
      this.router.navigate(['country', { categoryId: categoryId }]);
    }

    this.categories$.subscribe((categories) => {
      categories.forEach((category) => {
        if (category.id === categoryId) {
          this.store.dispatch(fromStore.SelectCategory({ payload: category }));
        }
      });
    });
    this.selectedCountry$.subscribe(
      (country) => (this.selectedCountry = country)
    );

    this.ownCountries$.subscribe((countries) => {
      countries.forEach((country) => {
        if (country.language_key === countryLanguageKey) {
          this.store.dispatch(fromStore.SelectCountry({ payload: country }));
        }
      });
    });

    this.store.dispatch(fromStore.LoadCountriesAction());
  }

  onCreateNewPhrase(phrase: NewPhraseResource): void {
    this.store.dispatch(CreateNewPhraseAction({ payload: phrase }));
  }

  onSelectRightPhrase(phrase: PhraseResource): void {
    const nextLeftMessage = Utils.getTranslationByCountry(
      phrase,
      this.selectedCountry
    );

    this.leftMessages = [
      ...this.leftMessages,
      ...[{ text: nextLeftMessage, own: false }],
    ];
    this.rightMessages = [
      ...this.rightMessages,
      ...[{ text: phrase.name, own: true }],
    ];
  }

  onSelectLeftPhrase(phrase: PhraseResource): void {
    const nextLeftMessage = Utils.getTranslationByCountry(
      phrase,
      this.selectedCountry
    );

    this.leftMessages = [
      ...this.leftMessages,
      ...[{ text: nextLeftMessage, own: true }],
    ];
    this.rightMessages = [
      ...this.rightMessages,
      ...[{ text: phrase.name, own: false }],
    ];
  }
}
