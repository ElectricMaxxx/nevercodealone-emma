import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromServices from './services';

import { AppComponent } from './components';

// not used in production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';

import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../environments/environment';
import { SharedFunctionsModule } from '@nevercodealone/shared-functions';
export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];

import { reducers, effects } from './store';
import { ServiceWorkerModule } from '@angular/service-worker';

import { appRoutingModule } from './app.routing';

import { NgSelectModule } from '@ng-select/ng-select';
import { adminUiRoutes, AdminUiModule } from '@nevercodealone/admin-ui';
import { DataModelsModule } from '@nevercodealone/data-models';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot([{ path: 'admin', children: adminUiRoutes }], {
      initialNavigation: 'enabled',
    }),
    StoreModule.forRoot({}, { metaReducers }),
    StoreModule.forFeature('app', reducers),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot(effects),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    appRoutingModule,
    RouterModule,
    NgSelectModule,
    AdminUiModule,
    DataModelsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatListModule,
    MatGridListModule,
    SharedFunctionsModule,
  ],
  providers: [[...fromServices.services]],
  bootstrap: [AppComponent],
})
export class AppModule {}
