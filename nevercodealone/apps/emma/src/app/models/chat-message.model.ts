export interface ChatMessageModel {
  text: string;
  own: boolean;
}
