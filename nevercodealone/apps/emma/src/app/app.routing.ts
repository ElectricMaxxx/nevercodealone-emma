import { Routes, RouterModule } from '@angular/router';

import {
  LoginComponent,
  HomeComponent,
  CountrySelectionComponent,
  CategorySelectionComponent,
  AdminComponent,
} from './components';
import { ChatComponent } from './containers';
import { Role } from '@nevercodealone/data-models';
import { AuthGuard } from '@nevercodealone/shared-functions';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  {
    path: 'chat/:categoryId/:countryLanguageKey',
    component: ChatComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'country/:categoryId',
    component: CountrySelectionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'category',
    component: CategorySelectionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.OrganisationAdmin, Role.User] },
  },
  { path: 'login', component: LoginComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

export const appRoutingModule = RouterModule.forRoot(routes, {
  relativeLinkResolution: 'legacy',
});
