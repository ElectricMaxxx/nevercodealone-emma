import * as fromPhrases from './phrases.action';

describe('PhrasesActions', () => {
  describe('LoadPhrases', () => {
    it('should create a LoadPhrasesAction', () => {
      expect(fromPhrases.LoadPhrasesAction.type).toEqual(
        '[phrases] Load phrases'
      );
    });
  });
  describe('CreateNewPhrase', () => {
    it('should create a CreateNewPhraseAction', () => {
      const payload = {
        name: 'test-name',
        category: { id: 1, category: 'test-category' },
      };
      const action = fromPhrases.CreateNewPhraseAction({ payload });

      expect(action.type).toEqual('[phrases] create new phrase');
      expect(action.payload).toEqual(payload);
    });
    it('should create a CreateNewPhraseSuccessAction', () => {
      const payload = {
        id: 1,
        name: 'test-name',
        category: { id: 1, category: 'test-category' },
        translations: [],
      };
      const action = fromPhrases.CreateNewPhraseSuccessAction({ payload });

      expect(action.type).toEqual('[phrases] create new phrase succeded');
      expect(action.payload).toEqual(payload);
    });
    it('should create a CreateNewPhraseSuccessAction', () => {
      const fail = 'fail';
      const action = fromPhrases.CreateNewPhraseFailAction({ payload: fail });

      expect(action.type).toEqual('[phrases] create new phrase failed');
      expect(action.payload).toEqual({ payload: fail });
    });
  });
});
