import { Action, createAction, props } from '@ngrx/store';
import { Organisation } from '@nevercodealone/data-models';

export const LOAD_ORGANISATION = '[organisation] Load organisation';
export const LOAD_ORGANISATION_SUCCESS =
  '[organisation] Load organisation succeeded';
export const LOAD_ORGANISATION_FAIL = '[organisation] Load organisation failed';
export const REMOVE_ORGANISATION_FROM_STORE =
  '[organisation] remove organisation from store';

export const LoadOrganisationAction = createAction(LOAD_ORGANISATION);
export const LoadOrganisationSuccessAction = createAction(
  LOAD_ORGANISATION_SUCCESS,
  props<{ payload: Organisation }>()
);
export const LoadOrganisationFailAction = createAction(
  LOAD_ORGANISATION_FAIL,
  props<{ payload: any }>()
);
export const RemoveOrganisationFromStoreAction = createAction(
  REMOVE_ORGANISATION_FROM_STORE
);
