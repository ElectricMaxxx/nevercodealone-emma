import { createAction } from '@ngrx/store';

export const UserLoginAction = createAction('[app] user login');
export const UserLogoutAction = createAction('[app] user logout');
