import { SelectCountry } from './view.action';

describe('SelectViewAction', () => {
  describe('SelectCountry', () => {
    it('should create a valid SelectCountry object', () => {
      const action = SelectCountry({
        payload: {
          id: 1,
          name: 'test',
          language_key: 'de_DE',
        },
      });

      expect(action.type).toEqual('[view] select a contry.');
      expect(action.payload).toEqual({ name: 'test', language_key: 'de_DE' });
    });
  });
});
