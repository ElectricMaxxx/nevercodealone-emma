import * as fromOrganisation from './organisation.action';

describe('OrganisationActions', () => {
  describe('Get Organisation Action', () => {
    it('should create a LoadOrganisationAction', () => {
      const action = fromOrganisation.LoadOrganisationAction;
      expect(action.type).toEqual(fromOrganisation.LOAD_ORGANISATION);
    });
  });
});
