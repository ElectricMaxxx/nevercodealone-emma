import { createAction, props } from '@ngrx/store';
import { CategoryResource, Country } from '@nevercodealone/data-models';
export const SelectCountry = createAction(
  '[view] select a contry.',
  props<{ payload: Country | null }>()
);
export const SelectCategory = createAction(
  '[view] select a categeory.',
  props<{ payload: CategoryResource | null }>()
);
