import * as fromCategories from './categories.action';

describe('CategoriesActions', () => {
  describe('LoadCategories', () => {
    it('should create a LoadCategoriesAction', () => {
      expect(fromCategories.LoadCategoriesAction.type).toEqual(
        '[categories] Load categories'
      );
    });
  });
  describe('CreateNewCategory', () => {
    it('should create a CreateNewCategoryAction', () => {
      const payload = { category: 'test-category', description: 'test-desc' };
      const action = fromCategories.CreateNewCategoryAction({ payload });

      expect(action.type).toEqual('[categories] create new category');
      expect(action.payload).toEqual(payload);
    });
    it('should create a CreateNewCategorySuccessAction', () => {
      const payload = {
        id: 1,
        category: 'test-category',
        description: 'test-desc',
      };
      const action = fromCategories.CreateNewCategorySuccessAction({ payload });

      expect(action.type).toEqual('[categories] create new category succeeded');
      expect(action.payload).toEqual(payload);
    });
    it('should create a CreateNewCategoryFailAction', () => {
      const fail = 'fail';
      const action = fromCategories.CreateNewCategoryFailAction({
        payload: fail,
      });

      expect(action.type).toEqual('[categories] create new category failed');
      expect(action.payload).toEqual(fail);
    });
  });
});
