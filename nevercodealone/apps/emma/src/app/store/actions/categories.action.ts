import { createAction, props } from '@ngrx/store';
import {
  CategoryResource,
  NewCategoryResource,
} from '@nevercodealone/data-models';
export const LoadCategoriesAction = createAction(
  '[categories] Load categories'
);
export const LoadCategoriesSuccessAction = createAction(
  '[categories] Load categories succeeded',
  props<{ payload: CategoryResource[] }>()
);
export const LoadCategoriesFailAction = createAction(
  '[categories] Load categories failed',
  props<{ payload: any }>()
);

export const CreateNewCategoryAction = createAction(
  '[categories] create new category',
  props<{ payload: NewCategoryResource }>()
);
export const CreateNewCategorySuccessAction = createAction(
  '[categories] create new category succeeded',
  props<{ payload: CategoryResource }>()
);
export const CreateNewCategoryFailAction = createAction(
  '[categories] create new category failed',
  props<{ payload: any }>()
);

export const RemoveCategoriesFromStoreAction = createAction(
  '[categories] remove categories from store'
);
