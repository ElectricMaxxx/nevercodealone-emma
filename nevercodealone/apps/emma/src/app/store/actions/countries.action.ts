import { createAction, props } from '@ngrx/store';
import { Country } from '@nevercodealone/data-models';

export const AddNewCountryToSelctionAction = createAction(
  '[country] add new country to own selection',
  props<{ payload: Country }>()
);
export const AddNewCountryToSelctionSuccessAction = createAction(
  '[country] adding new country to own selection succeded',
  props<{ payload: Country }>()
);

export const AddNewCountryToSelctionFailedAction = createAction(
  '[country] adding new country to own selection failed',
  props<{ payload: any }>()
);

export const LoadCountriesAction = createAction(
  '[country] load own countries available for organisation'
);

export const LoadCountriesActionSuccessAction = createAction(
  '[country] load own countries available for organisation succeded',
  props<{ payload: Country[] }>()
);
export const LoadCountriesActionFailedAction = createAction(
  '[country] load own countries available for organisation failed',
  props<{ payload: any }>()
);
export const LoadAllCountriesAction = createAction(
  '[country] load all countries available'
);
export const LoadAllCountriesActionSuccessAction = createAction(
  '[country] load all countries available succeded',
  props<{ payload: Country[] }>()
);
export const LoadAllCountriesActionFailedAction = createAction(
  '[country] load all countries available failed',
  props<{ payload: any }>()
);
