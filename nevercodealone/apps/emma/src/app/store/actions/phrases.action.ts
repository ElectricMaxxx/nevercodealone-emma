import { createAction, props } from '@ngrx/store';
import { NewPhraseResource, PhraseResource } from '@nevercodealone/data-models';

export const LoadPhrasesAction = createAction('[phrases] Load phrases');
export const LoadPhrasesSuccessAction = createAction(
  '[phrases] Load phrases succeded',
  props<{ payload: PhraseResource[] }>()
);
export const LoadPhrasesFailAction = createAction(
  '[phrases] Load phrases failed',
  props<{ payload: any }>()
);

export const CreateNewPhraseAction = createAction(
  '[phrases] create new phrase',
  props<{ payload: NewPhraseResource }>()
);
export const CreateNewPhraseSuccessAction = createAction(
  '[phrases] create new phrase succeded',
  props<{ payload: PhraseResource }>()
);
export const CreateNewPhraseFailAction = createAction(
  '[phrases] create new phrase failed',
  props<{ payload: any }>()
);
export const RemovePhrasesFromStoreAction = createAction(
  '[phrases} remove phrases from store'
);
