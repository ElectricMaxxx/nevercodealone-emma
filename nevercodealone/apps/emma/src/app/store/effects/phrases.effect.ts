import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as phrasesAction from '../actions/phrases.action';
import * as fromServices from '../../services';

@Injectable()
export class PhrasesEffect {
  constructor(
    private actions$: Actions,
    private phrasesService: fromServices.PhraseService
  ) {}

  loadPhrases$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(phrasesAction.LoadPhrasesAction),
        switchMap(() => {
          return this.phrasesService.getList().pipe(
            map((payload) =>
              phrasesAction.LoadPhrasesSuccessAction({ payload })
            ),
            catchError((payload) =>
              of(phrasesAction.LoadPhrasesFailAction({ payload }))
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  createNewPhrase$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(phrasesAction.CreateNewPhraseAction),
        switchMap((action) => {
          return this.phrasesService.addPhrase(action.payload).pipe(
            map((payload) =>
              phrasesAction.CreateNewPhraseSuccessAction({ payload })
            ),
            catchError((payload) =>
              of(phrasesAction.CreateNewPhraseFailAction({ payload }))
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );
}
