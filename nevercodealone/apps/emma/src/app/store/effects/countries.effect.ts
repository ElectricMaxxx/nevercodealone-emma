import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as countriesAction from '../actions/countries.action';
import * as fromServices from '../../services';
import { Country } from '@nevercodealone/data-models';
import { of } from 'rxjs';

@Injectable()
export class CountriesEffect {
  constructor(
    private actions$: Actions,
    private countryService: fromServices.CountryService
  ) {}

  loadCountries$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(countriesAction.LoadCountriesAction),
        switchMap(() => {
          return this.countryService.getList().pipe(
            map((countries: Country[]) =>
              countriesAction.LoadCountriesActionSuccessAction({
                payload: countries,
              })
            ),
            catchError((error) =>
              of(
                countriesAction.LoadCountriesActionFailedAction({
                  payload: error,
                })
              )
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  loadAllCountries$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(countriesAction.LoadAllCountriesAction),
        switchMap(() => {
          return this.countryService.getAllList().pipe(
            map((countries: Country[]) =>
              countriesAction.LoadAllCountriesActionSuccessAction({
                payload: countries,
              })
            ),
            catchError((error) =>
              of(
                countriesAction.LoadAllCountriesActionFailedAction({
                  payload: error,
                })
              )
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  createNewCountry$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(countriesAction.AddNewCountryToSelctionAction),
        switchMap((action) => {
          return this.countryService.addNewCountry(action.payload).pipe(
            map((newCountry: Country) =>
              countriesAction.AddNewCountryToSelctionSuccessAction({
                payload: newCountry,
              })
            ),
            catchError((error) =>
              of(
                countriesAction.AddNewCountryToSelctionFailedAction({
                  payload: error,
                })
              )
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );
}
