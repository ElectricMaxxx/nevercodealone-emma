import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as organisationAction from '../actions/organisation.action';
import * as categoryAction from '../actions/categories.action';
import * as phraseAction from '../actions/phrases.action';
import * as fromServices from '../../services';

@Injectable()
export class OrganisationEffect {
  constructor(
    private actions$: Actions,
    private organisationService: fromServices.OrganisationService
  ) {}

  getOrganisation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(organisationAction.LOAD_ORGANISATION),
        switchMap(() => {
          return this.organisationService.getOrganisation().pipe(
            map((payload) =>
              organisationAction.LoadOrganisationSuccessAction({ payload })
            ),
            catchError((payload) =>
              of(organisationAction.LoadOrganisationFailAction({ payload }))
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  UserLogin$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType('[app] user login'),
        switchMap(() => {
          return of({}).pipe(
            map(() => organisationAction.LoadOrganisationAction()),
            map(() => phraseAction.LoadPhrasesAction()),
            map(() => categoryAction.LoadCategoriesAction())
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  UserLogout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType('[app] user logout'),
        switchMap(() => {
          return of().pipe(
            map(() => organisationAction.RemoveOrganisationFromStoreAction())
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );
}
