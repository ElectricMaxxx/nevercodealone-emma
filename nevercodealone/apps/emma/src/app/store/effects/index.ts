import { CategoriesEffect } from './categories.effect';
import { PhrasesEffect } from './phrases.effect';
import { OrganisationEffect } from './organisation.effect';
import { AppEffect } from './app.effect';
import { CountriesEffect } from './countries.effect';

export const effects = [CategoriesEffect, PhrasesEffect, OrganisationEffect, AppEffect, CountriesEffect];

export * from './categories.effect';
export * from './phrases.effect';
export * from './organisation.effect';
export * from './app.effect';
export * from './countries.effect';
