import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as categoriesAction from '../actions/categories.action';
import * as fromServices from '../../services';
import { CategoryResource } from '@nevercodealone/data-models';

@Injectable()
export class CategoriesEffect {
  constructor(
    private actions$: Actions,
    private categoryService: fromServices.CategoryService
  ) {}

  loadCategories$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(categoriesAction.LoadCategoriesAction),
        switchMap(() => {
          return this.categoryService.getList().pipe(
            map((categories: CategoryResource[]) =>
              categoriesAction.LoadCategoriesSuccessAction({
                payload: categories,
              })
            ),
            catchError((error) =>
              of(categoriesAction.LoadCategoriesFailAction({ payload: error }))
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );

  createNewCategory$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(categoriesAction.CreateNewCategoryAction),
        switchMap((action) => {
          return this.categoryService.addCategory(action.payload).pipe(
            map((newCategory: CategoryResource) =>
              categoriesAction.CreateNewCategorySuccessAction({
                payload: newCategory,
              })
            ),
            catchError((error) =>
              of(
                categoriesAction.CreateNewCategoryFailAction({ payload: error })
              )
            )
          );
        })
      ),
    { useEffectsErrorHandler: false }
  );
}
