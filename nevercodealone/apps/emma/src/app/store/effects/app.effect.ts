import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import * as organisationAction from '../actions/organisation.action';
import * as categoryAction from '../actions/categories.action';
import * as phraseAction from '../actions/phrases.action';
import * as fromApp from '../actions/app.action';

@Injectable()
export class AppEffect {
  constructor(private actions$: Actions) {}

  UserLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApp.UserLoginAction),
      switchMap(() => {
        return of({}).pipe(
          map(() => organisationAction.LoadOrganisationAction()),
          map(() => phraseAction.LoadPhrasesAction()),
          map(() => categoryAction.LoadCategoriesAction())
        );
      })
    )
  );

  UserLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApp.UserLogoutAction),
      switchMap(() => {
        return of().pipe(
          map(() => organisationAction.RemoveOrganisationFromStoreAction()),
          map(() => phraseAction.RemovePhrasesFromStoreAction()),
          map(() => categoryAction.RemoveCategoriesFromStoreAction())
        );
      })
    )
  );
}
