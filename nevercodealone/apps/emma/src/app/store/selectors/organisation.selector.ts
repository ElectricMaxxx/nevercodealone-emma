import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromOrganisation from '../reducers/organisation.reducer';

export const getOrganisationState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.organisation
);

export const getOrganisationEntity = createSelector(
    getOrganisationState,
    fromOrganisation.getEntity
);

export const getOrganisationLoaded = createSelector(
    getOrganisationState,
    fromOrganisation.getLoaded
);
export const getOrganisationLoading = createSelector(
    getOrganisationState,
    fromOrganisation.getLoading
);
