import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromCountries from '../reducers/countries.reducer';

export const getCountriesState = createSelector(
  fromFeature.getAppState,
  (state: fromFeature.AppState) => state.countries
);

export const getCountryEntities = createSelector(
  getCountriesState,
  fromCountries.getCountryEntities
);

export const getAllCountryEntities = createSelector(
  getCountriesState,
  fromCountries.getAllCountryEntities
);

export const getCountries = createSelector(getCountryEntities, (entities) => {
  return Object.keys(entities).map((key) => entities[key]);
});

export const getAllCountries = createSelector(
  getAllCountryEntities,
  (entities) => {
    return Object.keys(entities).map((key: string) => entities[key]);
  }
);

export const getCountriesLoaded = createSelector(
  getCountriesState,
  fromCountries.getLoaded
);
export const getCountriesLoading = createSelector(
  getCountriesState,
  fromCountries.getLoading
);
