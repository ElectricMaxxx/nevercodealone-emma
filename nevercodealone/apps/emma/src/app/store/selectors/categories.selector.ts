import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromCategories from '../reducers/categories.reducer';

export const getCategoriesState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.categories
);

export const getCategoryEntities = createSelector(
    getCategoriesState,
    fromCategories.getAllCategoryEntities
);

export const getAllCategories = createSelector(getCategoryEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getCategoriesLoaded = createSelector(
    getCategoriesState,
    fromCategories.getLoaded
);
export const getCategoriesLoading = createSelector(
    getCategoriesState,
    fromCategories.getLoading
);
