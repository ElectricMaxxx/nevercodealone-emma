import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromPhrases from '../reducers/phrases.reducer';

export const getPhrasesState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.phrases
);

export const getPhraseEntities = createSelector(
    getPhrasesState,
    fromPhrases.getAllPhraseEntities
);

export const getAllPhrases = createSelector(getPhraseEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getPhrasesLoaded = createSelector(
    getPhrasesState,
    fromPhrases.getLoaded
);
export const getPhrasesLoading = createSelector(
    getPhrasesState,
    fromPhrases.getLoading
);
