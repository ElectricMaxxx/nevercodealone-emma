import * as fromCountries from '../actions/countries.action';
import { Country } from '@nevercodealone/data-models';
import { createReducer, on } from '@ngrx/store';

export interface CountriesState {
  entities: { [id: string]: Country };
  all_entities: { [id: string]: Country };
  loaded: boolean;
  loading: boolean;
}

export const initialState: CountriesState = {
  entities: {},
  all_entities: {},
  loaded: false,
  loading: false,
};
export const reducer = createReducer(
  initialState,
  on(
    fromCountries.LoadCountriesAction,
    fromCountries.LoadAllCountriesAction,
    (state) => ({
      ...state,
      loading: true,
    })
  ),
  on(fromCountries.LoadCountriesActionSuccessAction, (state, action) => ({
    ...state,
    entities: action.payload.reduce(
      (entities: { [id: string]: Country }, mappedCountry: Country) => {
        return { ...entities, [mappedCountry.language_key]: mappedCountry };
      },
      { ...state.entities }
    ),
    loaded: true,
    loading: false,
  })),
  on(fromCountries.LoadAllCountriesActionSuccessAction, (state, action) => ({
    ...state,
    all_entities: action.payload.reduce(
      (entities: { [id: string]: Country }, mappedCountry: Country) => {
        return { ...entities, [mappedCountry.language_key]: mappedCountry };
      },
      { ...state.all_entities }
    ),
    loaded: true,
    loading: false,
  })),
  on(fromCountries.AddNewCountryToSelctionSuccessAction, (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      [action.payload.language_key]: action.payload,
    },
    loaded: false,
    loading: false,
  })),
  on(
    fromCountries.AddNewCountryToSelctionFailedAction,
    fromCountries.LoadCountriesActionFailedAction,
    fromCountries.LoadAllCountriesActionFailedAction,
    (state, action) => ({
      ...state,
      loading: false,
      loaded: false,
    })
  ),
  on(fromCountries.AddNewCountryToSelctionAction, (state) => ({
    ...state,
    loading: true,
  }))
);

export const getCountryEntities = (state: CountriesState) => state.entities;
export const getAllCountryEntities = (state: CountriesState) =>
  state.all_entities;
export const getLoading = (state: CountriesState) => state.loading;
export const getLoaded = (state: CountriesState) => state.loaded;
