import * as fromPhrases from '../actions/phrases.action';
import { PhraseResource } from '@nevercodealone/data-models';
import { createReducer, on } from '@ngrx/store';
import { from } from 'rxjs';

export interface PhrasesState {
  entities: { [id: number]: PhraseResource };
  loaded: boolean;
  loading: boolean;
}

export const initialState: PhrasesState = {
  entities: {},
  loaded: false,
  loading: false,
};
export const reducer = createReducer(
  initialState,
  on(
    fromPhrases.CreateNewPhraseAction,
    fromPhrases.LoadPhrasesAction,
    (state) => ({
      ...state,
      loading: true,
    })
  ),
  on(fromPhrases.CreateNewPhraseSuccessAction, (state, action) => ({
    ...state,
    entities: {
      ...state.entities,
      [String(action.payload.id)]: action.payload,
    },
    loaded: false,
    loading: false,
  })),
  on(fromPhrases.LoadPhrasesSuccessAction, (state, action) => ({
    ...state,
    entities: action.payload.reduce(
      (entities: { [id: number]: PhraseResource }, phrase: PhraseResource) => {
        return { ...entities, [String(phrase.id)]: phrase };
      },
      { ...state.entities }
    ),
    loaded: true,
    loading: false,
  })),
  on(fromPhrases.RemovePhrasesFromStoreAction, (state) => ({
    ...state,
    entities: [],
    loading: false,
    loaded: false,
  })),
  on(
    fromPhrases.CreateNewPhraseFailAction,
    fromPhrases.LoadPhrasesFailAction,
    (state) => ({
      ...state,
      loading: false,
      loaded: false,
    })
  )
);

export const getAllPhraseEntities = (
  state: PhrasesState
): { [id: number]: PhraseResource } => state.entities;
export const getLoading = (state: PhrasesState) => state.loading;
export const getLoaded = (state: PhrasesState) => state.loaded;
