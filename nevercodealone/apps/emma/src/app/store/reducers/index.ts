import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromCategories from './categories.reducer';
import * as fromCountries from './countries.reducer';
import * as fromPhrases from './phrases.reducer';
import * as fromSelected from './view.reducer';
import * as fromOrganisation from './organisation.reducer';

export interface AppState {
    phrases: fromPhrases.PhrasesState;
    categories: fromCategories.CategoriesState;
    view: fromSelected.ViewState;
    organisation: fromOrganisation.OrganisationState;
    countries: fromCountries.CountriesState;
}

export const reducers: ActionReducerMap<AppState> = {
    categories: fromCategories.reducer,
    phrases: fromPhrases.reducer,
    view: fromSelected.reducer,
    organisation: fromOrganisation.reducer,
    countries: fromCountries.reducer
};

export const getAppState = createFeatureSelector<AppState>('app');
