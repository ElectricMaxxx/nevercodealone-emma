import * as fromOrganisation from '../actions/organisation.action';
import { Organisation } from '@nevercodealone/data-models';
import { createReducer, on } from '@ngrx/store';

export interface OrganisationState {
  entity: Organisation | null;
  loaded: boolean;
  loading: boolean;
}

export const initialState: OrganisationState = {
  entity: null,
  loaded: false,
  loading: false,
};

export const reducer = createReducer(
  initialState,
  on(fromOrganisation.LoadOrganisationAction, (state) => ({
    ...state,
    loading: true,
  })),
  on(fromOrganisation.LoadOrganisationSuccessAction, (state, action) => ({
    ...state,
    entity: action.payload,
    loaded: true,
    loading: false,
  })),
  on(fromOrganisation.LoadOrganisationFailAction, (state) => ({
    ...state,
    loading: false,
    loaded: false,
  })),
  on(fromOrganisation.RemoveOrganisationFromStoreAction, (state) => ({
    ...state,
    entity: null,
    loading: false,
    loaded: false,
  }))
);

export const getEntity = (state: OrganisationState) => state.entity;
export const getLoading = (state: OrganisationState) => state.loading;
export const getLoaded = (state: OrganisationState) => state.loaded;
