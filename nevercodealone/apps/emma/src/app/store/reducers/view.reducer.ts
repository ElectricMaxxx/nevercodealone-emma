import * as fromView from '../actions/view.action';
import { CategoryResource, Country } from '@nevercodealone/data-models';
import { createReducer, on } from '@ngrx/store';

export interface Selected {
  country: Country | null;
  category: CategoryResource | null;
}

export interface ViewState {
  selected: Selected;
}

const initialSelected: Selected = {
  country: null,
  category: null,
};

export const initialState: ViewState = {
  selected: initialSelected,
};

export const reducer = createReducer(
  initialState,
  on(fromView.SelectCountry, (state, action) => ({
    ...state,
    selected: { ...state.selected, country: action.payload },
  })),
  on(fromView.SelectCategory, (state, action) => ({
    ...state,
    selected: { ...state.selected, category: action.payload },
  }))
);

export const getSelected = (state: ViewState) => state.selected;
