import * as fromCategories from '../actions/categories.action';
import { CategoryResource } from '@nevercodealone/data-models';
import { createReducer, on } from '@ngrx/store';

export interface CategoriesState {
  entities: { [id: number]: CategoryResource };
  loaded: boolean;
  loading: boolean;
}

export const initialState: CategoriesState = {
  entities: {},
  loaded: false,
  loading: false,
};

export const reducer = createReducer(
  initialState,
  on(
    fromCategories.CreateNewCategoryAction,
    fromCategories.LoadCategoriesAction,
    (state) => ({
      ...state,
      loading: true,
    })
  ),
  on(
    fromCategories.LoadCategoriesSuccessAction,
    (state: CategoriesState, action) => ({
      ...state,
      entities: action.payload.reduce(
        (
          entities: { [id: number]: CategoryResource },
          category: CategoryResource
        ) => {
          return { ...entities, [category.id]: category };
        },
        { ...state.entities }
      ),
      loaded: true,
      loading: false,
    })
  ),
  on(fromCategories.CreateNewCategorySuccessAction, (state, action) => ({
    ...state,
    entities: { ...state.entities, [action.payload.id]: action.payload },
    loaded: false,
    loading: false,
  })),
  on(
    fromCategories.LoadCategoriesFailAction,
    fromCategories.CreateNewCategoryFailAction,
    (state) => ({
      ...state,
      loading: false,
      loaded: false,
    })
  ),
  on(fromCategories.RemoveCategoriesFromStoreAction, (state) => ({
    ...state,
    entities: [],
    loading: false,
    loaded: false,
  }))
);

export const getAllCategoryEntities = (state: CategoriesState) =>
  state.entities;
export const getLoading = (state: CategoriesState) => state.loading;
export const getLoaded = (state: CategoriesState) => state.loaded;
