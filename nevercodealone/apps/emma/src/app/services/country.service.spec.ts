import { CountryService } from './country.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('CategoryService', () => {
  let service: CountryService;
  let httpClientMock: HttpClient | any;
  beforeEach(() => {
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
    service = new CountryService(httpClientMock as HttpClient);
  });

  it('should call get method on http client', async () => {
    const stubValue = [{ name: 'test-country', language_key: 'de_DE' }];
    httpClientMock.get.and.returnValue(of(stubValue));
    await service.getList().subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });
});
