import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Role, User } from '@nevercodealone/data-models';
import { Observable } from 'rxjs/Observable';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiUrl}/users`);
  }

  getById(id: number): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/users/${id}`);
  }
}
