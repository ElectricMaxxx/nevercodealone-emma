import {
  PhraseResource,
  Country,
  Translation,
} from '@nevercodealone/data-models';
import { Injectable } from '@angular/core';

@Injectable()
export class Utils {
  static getTranslationByCountry(
    phrase: PhraseResource,
    country: Country | null
  ): string {
    if (null === country) {
      return phrase.name;
    }
    const translations = phrase.translations.filter(
      (translation: Translation) => {
        return translation.language_key === country.language_key;
      }
    );
    if (translations.length === 1) {
      const translation = translations.shift() as Translation;
      return translation.translation;
    }
    return phrase.name;
  }
}
