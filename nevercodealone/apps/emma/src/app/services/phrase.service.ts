import { Inject, Injectable } from '@angular/core';
import { NewPhraseResource, PhraseResource } from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';

@Injectable()
export class PhraseService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  private generateIRI(resource: string, id?: number): string {
    return this.apiUrl + '/phrases' + typeof id !== undefined
      ? '/' + id?.toString()
      : '';
  }

  getList(): Observable<PhraseResource[]> {
    return this.http.get<PhraseResource[]>(this.apiUrl + '/phrases');
  }

  getPhrase(id: number): Observable<PhraseResource> {
    return this.http.get<PhraseResource>(this.apiUrl + '/phrases' + '/' + id);
  }

  addPhrase(phrase: NewPhraseResource): Observable<PhraseResource> {
    return this.http.post<PhraseResource>(this.apiUrl + '/phrases', {
      name: phrase.name,
      category: phrase.category.id,
    });
  }

  removePhrase(phrase: PhraseResource): Observable<PhraseResource> {
    return this.http.delete<PhraseResource>(
      this.apiUrl + '/phrases' + '/' + phrase.id
    );
  }
}
