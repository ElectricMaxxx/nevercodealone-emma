import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { OrganisationService } from './organisation.service';

describe('OrganisationService', () => {
  let service: OrganisationService;
  let httpClientMock: HttpClient | any;
  beforeEach(() => {
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
    service = new OrganisationService(httpClientMock);
  });

  it('should call get method on http client', async () => {
    const stubValue = {
      name: 'username',
      id: 1,
      categories: [],
      phrases: [],
      user: null,
      countries: [],
    };
    httpClientMock.get.and.returnValue(of([stubValue]));
    await service.getOrganisation().subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });
});
