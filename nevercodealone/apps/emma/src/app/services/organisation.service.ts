import { Inject, Injectable } from '@angular/core';
import { Organisation } from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError, first } from 'rxjs/operators';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';
import { throwError } from 'rxjs';

@Injectable()
export class OrganisationService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  getOrganisation(): Observable<Organisation> {
    return this.http.get<Organisation>(this.apiUrl + `/organisation`).pipe(
      catchError((error: unknown) => throwError(error)),
      first()
    );
  }

  getAdminOrganisations(): Observable<Organisation[]> {
    return this.http.get<Organisation[]>(this.apiUrl + '/admin/organisations');
  }
  postAdminOrganisations(organisation: Organisation): Observable<Organisation> {
    return this.http.post<Organisation>(
      this.apiUrl + '/admin/organisations',
      organisation
    );
  }
  patchAdminOrganisations(
    organisation: Organisation
  ): Observable<Organisation> {
    return this.http
      .patch<Organisation>(
        `${this.apiUrl}/admin/organisations/${organisation.id}`,
        organisation
      )
      .pipe(catchError((error: unknown) => throwError(error)));
  }
}
