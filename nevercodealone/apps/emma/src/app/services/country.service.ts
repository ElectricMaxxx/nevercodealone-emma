import { Inject, Injectable } from '@angular/core';
import { Country } from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { throwError } from 'rxjs';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';
@Injectable()
export class CountryService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  getList(): Observable<Country[]> {
    return this.http
      .get<Country[]>(this.apiUrl + `/countries`)
      .pipe(catchError((error: any) => throwError(error)));
  }

  getAllList(): Observable<Country[]> {
    return this.http
      .get<Country[]>(this.apiUrl + `/all-countries`)
      .pipe(catchError((error: any) => throwError(error)));
  }

  addNewCountry(country: Country): Observable<Country> {
    return this.http
      .post<Country>(this.apiUrl + `/countries`, {
        name: country.name,
        language_key: country.language_key,
      })
      .pipe(catchError((error: any) => throwError(error)));
  }
}
