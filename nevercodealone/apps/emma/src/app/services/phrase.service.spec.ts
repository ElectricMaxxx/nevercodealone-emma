import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { PhraseService } from './phrase.service';

describe('PhraseService', () => {
  let service: PhraseService;
  let httpClientMock: HttpClient | any;
  beforeEach(() => {
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    service = new PhraseService(httpClientMock);
  });

  it('should call get method on http client', async () => {
    const stubValue = [
      {
        name: 'test-message',
        category: { category: 'text-category' },
        translations: [],
      },
    ];
    httpClientMock.get.and.returnValue(of(stubValue));
    await service.getList().subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });

  it('should call post method on http client', async () => {
    const phrase = {
      id: 1,
      name: 'test-message',
      category: { id: 1, category: 'text-category' },
      translations: [],
    };
    const stubValue = [{ ...phrase }];
    httpClientMock.post.and.returnValue(of(stubValue));
    await service.addPhrase(phrase).subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });
});
