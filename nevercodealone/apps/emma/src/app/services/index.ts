import { CountryService } from './country.service';
import { PhraseService } from './phrase.service';
import { CategoryService } from './category.service';
import { TranslationService } from './translation.service.';
import { OrganisationService } from './organisation.service';
import { Utils } from './utils.service';
import {
  ENV_MODE_PRODUCTION,
  HTTP_API_URL,
  HTTP_BACKEND_URL,
  HTTP_SERVICE,
  SimpleHttpService,
} from '@nevercodealone/shared-functions';
import { environment } from '../../environments/environment';

export const services: any[] = [
  CountryService,
  PhraseService,
  CategoryService,
  TranslationService,
  OrganisationService,
  Utils,
  {
    provide: HTTP_SERVICE,
    useClass: SimpleHttpService,
  },
  { provide: HTTP_API_URL, useValue: environment.apiUrl },
  { provide: HTTP_BACKEND_URL, useValue: environment.backendUrl },
  { provide: ENV_MODE_PRODUCTION, useValue: environment.production },
];

export * from './country.service';
export * from './phrase.service';
export * from './category.service';
export * from './translation.service.';
export * from './organisation.service';
export * from './utils.service';
