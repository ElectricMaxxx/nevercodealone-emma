import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { TranslationService } from '.';

describe('TranslationService', () => {
  let service: TranslationService;
  let httpClientMock: HttpClient | any;
  beforeEach(() => {
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
    service = new TranslationService(httpClientMock);
  });

  it('should call get method on http client', async () => {
    const stubValue = [{ translation: 'test-message', language_key: 'de_DE' }];
    httpClientMock.get.and.returnValue(of(stubValue));
    await service.getList().subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });
});
