import { Inject, Injectable } from '@angular/core';
import {
  CategoryResource,
  NewCategoryResource,
} from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';
import { throwError } from 'rxjs';

@Injectable()
export class CategoryService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  getList(): Observable<CategoryResource[]> {
    return this.http.get<CategoryResource[]>(this.apiUrl + `/categories`).pipe(
      catchError((error: unknown) => {
        return throwError(error);
      })
    );
  }

  addCategory(category: NewCategoryResource): Observable<CategoryResource> {
    return this.http
      .post<CategoryResource>(this.apiUrl + `/category`, {
        category: category.category,
        description: category.description,
      })
      .pipe(
        catchError((error: unknown) => {
          return throwError(error);
        })
      );
  }
}
