import { Inject, Injectable } from '@angular/core';
import { Translation } from '@nevercodealone/data-models';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { HTTP_API_URL } from '@nevercodealone/shared-functions';
import { throwError } from 'rxjs';

@Injectable()
export class TranslationService {
  constructor(
    private http: HttpClient,
    @Inject(HTTP_API_URL) private apiUrl: string
  ) {}

  getList(): Observable<Translation[]> {
    return this.http
      .get<Translation[]>(this.apiUrl + `/translations`)
      .pipe(catchError((error: unknown) => throwError(error)));
  }
}
