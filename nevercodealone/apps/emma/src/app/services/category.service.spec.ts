import { CategoryService } from './category.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('CategoryService', () => {
  let service: CategoryService;
  let httpClientMock: HttpClient | any;
  beforeEach(() => {
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
    service = new CategoryService(httpClientMock as HttpClient);
  });

  it('should call get method on http client', async () => {
    const stubValue = [{ category: 'test-message' }];
    httpClientMock.get.and.returnValue(of(stubValue));
    await service.getList().subscribe((value) => {
      expect(value).toBe(stubValue);
    });
  });
});
