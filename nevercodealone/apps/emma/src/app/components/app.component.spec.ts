import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthenticationService } from '../services';
import { of, Observable } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { Organisation } from '@nevercodealone/data-models';

const AuthenticationServiceStub = {
  currentUser: of({
    id: 1,
    name: 'username',
    password: 'abc',
    organisaton: { id: 1 },
    firstName: 'John',
    lastName: 'Doe',
    roles: ['USER_ADMIN'],
  }),
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  logout: () => {},
};
const organisation: Organisation = {
  id: 1,
  name: 'test-orga',
  user: { id: 1, username: 'test-user' },
  phrases: [],
  categories: [],
  countries: [],
};
const organisatonObservable: Observable<Organisation> = of(organisation);

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(
    waitForAsync(() => {
      const StoreStub = {
        select() {
          return {
            subscribe() {
              return organisatonObservable;
            },
          };
        },
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        dispatch() {},
      };

      TestBed.configureTestingModule({
        declarations: [AppComponent],
        providers: [
          { provide: Store, useValue: StoreStub },
          {
            provide: AuthenticationService,
            useValue: AuthenticationServiceStub,
          },
        ],
        schemas: [NO_ERRORS_SCHEMA],
        imports: [
          RouterTestingModule.withRoutes([
            { path: '', component: AppComponent },
          ]),
        ],
      }).compileComponents();
    })
  );
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it(
    'should create the app component',
    waitForAsync(() => {
      expect(component).toBeDefined();
    })
  );

  it(
    `should have as title 'EMMA'`,
    waitForAsync(() => {
      const HTML: HTMLElement = fixture.nativeElement;
      expect(HTML.querySelector('a.navbar-brand')?.innerHTML).toContain('EMMA');
    })
  );
});
