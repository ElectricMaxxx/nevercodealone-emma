import { Component, OnInit } from '@angular/core';
import { User } from '@nevercodealone/data-models';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'nevercodealone-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  loading = false;
  users: User[] = [];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.loading = true;
    this.userService.getAll().subscribe((users: User[]) => {
      this.loading = false;
      this.users = users;
    });
  }
}
