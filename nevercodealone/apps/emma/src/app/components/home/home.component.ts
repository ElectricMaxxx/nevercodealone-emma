import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { User } from '@nevercodealone/data-models';
import { AuthenticationService } from '@nevercodealone/shared-functions';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'nevercodealone-home',
  template: `
    <div class="container">
      <div
        class="sb-box sb-box-wide sb-bottom-left sb-warning"
        *ngIf="
          (selectedCountry$ | async) === null &&
          (selectedCategory$ | async) === null
        "
      >
        <h4>Kategorie/Station auswählen</h4>
        <p>Es ist noch keine Abteilung ausgewählt</p>
        <p>Bitte wähle hier: <a [routerLink]="['category']">Abteilung</a></p>
      </div>
      <div
        class="sb-box sb-box-wide sb-bottom-left sb-warning"
        *ngIf="
          (selectedCountry$ | async) === null && (selectedCategory$ | async)
        "
      >
        <h4>Sprach-Auswahl</h4>
        <p>Es ist noch keine Sprache Ihres Gegenübers gewählt.</p>
        <p>
          Bitte wähle hier:
          <a
            [routerLink]="[
              'country/' + (selectedCountry$ | async)?.language_key
            ]"
            >Sprachen</a
          >
        </p>
      </div>
    </div>
  `,
})
export class HomeComponent implements OnInit {
  loading = false;
  currentUser: User | null = null;

  selectedCountry$ = this.store.select(fromStore.getSelectedCountry);
  selectedCategory$ = this.store.select(fromStore.getSelectedCategory);

  public constructor(
    protected store: Store<fromStore.AppState>,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit(): void {
    this.store.dispatch(fromStore.LoadAllCountriesAction());
    this.store.dispatch(fromStore.LoadCategoriesAction());
  }
}
