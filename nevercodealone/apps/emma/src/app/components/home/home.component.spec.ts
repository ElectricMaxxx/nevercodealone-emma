import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  CountryService,
  CategoryService,
  TranslationService,
} from '../../services';

describe('HomeComponent', () => {
  let countryService: CountryService;
  let categoryService: CategoryService;
  let translationService: TranslationService;
  let fixture: ComponentFixture<HomeComponent>;
  let component: HomeComponent;
  const StoreStub = {
    select() {
      return {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        subscribe() {},
      };
    },
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    dispatch() {},
  };

  beforeEach(
    waitForAsync(() => {
      countryService = jasmine.createSpyObj('CountryService', ['getList']);
      categoryService = jasmine.createSpyObj('CategoryService', ['getList']);
      translationService = jasmine.createSpyObj('TranslationService', [
        'getList',
      ]);

      TestBed.configureTestingModule({
        declarations: [HomeComponent],
        providers: [
          { provide: Store, useValue: StoreStub },
          { provide: CountryService, useValue: countryService },
          { provide: CategoryService, useValue: categoryService },
          { provide: TranslationService, useValue: translationService },
        ],
        schemas: [NO_ERRORS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(
    'should create the app',
    waitForAsync(() => {
      expect(component).toBeDefined();
    })
  );
});
