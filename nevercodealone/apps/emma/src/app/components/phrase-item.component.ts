import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  Country,
  PhraseResource,
  Translation,
} from '@nevercodealone/data-models';

@Component({
  selector: 'nevercodealone-phrase-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card mb-1" *ngIf="phrase">
      <h6 class="card-header">{{ phrase.name }}</h6>
      <div class="card-body">
        <div class="card-text">
          <span *ngIf="translation">{{ translation.translation }}</span>
          <span class="text-danger" *ngIf="!translation">No Translation</span>
        </div>
      </div>
    </div>
  `,
})
export class PhraseItemComponent implements OnInit, OnChanges {
  @Input() phrase: PhraseResource | null = null;
  @Input() selectedCountry: Country | null = null;

  translation: Translation | null = null;

  ngOnInit(): void {
    this.evalutateTranslation();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.evalutateTranslation();
  }

  private evalutateTranslation(): void {
    if (!this.phrase || !this.selectedCountry) {
      return;
    }
    const translations = this.phrase.translations.filter(
      (translation) =>
        translation.language_key === this.selectedCountry?.language_key
    );
    if (!translations.length) {
      return;
    }

    this.translation = translations.shift() as Translation;
  }
}
