import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';
import { Country } from '@nevercodealone/data-models';
import * as fromStore from '../../store';
import { Store } from '@ngrx/store';
import { zip } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nevercodealone-country-selection',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './country.selection.component.html',
  styleUrls: ['country.selection.component.scss'],
})
export class CountrySelectionComponent implements OnInit {
  selectedCategoryId: number | null = null;
  selectedCategory$ = this.store.select(fromStore.getSelectedCategory);
  countries: { country: Country; addMode: boolean }[] = [];
  allVisible = false;

  categories$ = this.store.select(fromStore.getAllCategories);
  allCountries$ = this.store.select(fromStore.getAllCountries);
  ownCountries$ = this.store.select(fromStore.getCountries);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromStore.AppState>,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    if (null !== this.route.snapshot.paramMap) {
      const categoryIdParameter = this.route.snapshot.paramMap.get(
        'categoryId'
      );
      this.selectedCategoryId =
        null !== categoryIdParameter ? parseInt(categoryIdParameter, 10) : null;
    }

    if (!this.selectedCategoryId) {
      this.router.navigate(['category']);
    }

    this.categories$.subscribe((categories) => {
      categories.forEach((category) => {
        if (category.id === this.selectedCategoryId) {
          this.store.dispatch(fromStore.SelectCategory({ payload: category }));
        }
      });
    });

    zip(this.ownCountries$, this.allCountries$).subscribe((value) => {
      const ownCountries: Country[] = value[0];
      const countries: Country[] = value[1];
      const mergedCountries = {
        ...ownCountries
          .map((country) => ({
            country,
            addMode: false,
          }))
          .reduce(
            (
              prev: { [id: string]: { country: Country; addMode: boolean } },
              item: { country: Country; addMode: boolean }
            ): { [id: string]: { country: Country; addMode: boolean } } => {
              prev[item.country.language_key] = item;
              return prev;
            },
            {}
          ),
        ...countries
          .map((country) => ({
            country,
            addMode: true,
          }))
          .reduce(
            (
              prev: { [id: string]: { country: Country; addMode: boolean } },
              item: { country: Country; addMode: boolean }
            ): { [id: string]: { country: Country; addMode: boolean } } => {
              if (prev[item.country.language_key]) {
                return prev;
              }
              prev[item.country.language_key] = item;
              return prev;
            },
            {}
          ),
      };
      const allCountries = Object.keys(mergedCountries).map(
        (key: string) => mergedCountries[key]
      );
      this.countries = Array.from(allCountries);
      this.cd.detectChanges();
    });

    this.store.dispatch(fromStore.LoadCountriesAction());
    this.store.dispatch(fromStore.LoadAllCountriesAction());
    this.store.dispatch(fromStore.LoadCategoriesAction());
  }

  onSelectCountry(country: Country): void {
    this.store.dispatch(fromStore.SelectCountry({ payload: country }));

    this.router.navigate([
      'chat/' + this.selectedCategoryId + '/' + country.language_key,
    ]);
  }

  onAddNewCountry(payload: Country): void {
    this.store.dispatch(fromStore.AddNewCountryToSelctionAction({ payload }));
    this.onToggleAddMode();
  }

  onToggleAddMode(): void {
    this.allVisible = !this.allVisible;
  }
}
