import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CategoryResource, Country } from '@nevercodealone/data-models';
@Component({
  selector: 'nevercodealone-country-selection-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      class="sb-box sb-right sb-action"
      [class.add-mode]="addMode && country !== null"
      (click)="onSelectCountry()"
      title="Wähle {{ country?.name }}"
      style="background: url('assets/svg-country-flags/{{ country?.language_key }}.svg') no-repeat center"
    >
    </div>
  `,
})
export class CountrySelectionItemComponent {
  @Input() country: Country | null = null;
  @Input() addMode = false;
  @Input() category: CategoryResource | null = null;
  @Output() selectCountry: EventEmitter<Country> = new EventEmitter<Country>();
  @Output()
  addToselectableCountries: EventEmitter<Country> = new EventEmitter<Country>();
  onSelectCountry(): void {
    if (null === this.country) {
      return;
    }
    if (this.addMode) {
      this.addToselectableCountries.emit(this.country);
    } else {
      this.selectCountry.emit(this.country);
    }
  }
}
