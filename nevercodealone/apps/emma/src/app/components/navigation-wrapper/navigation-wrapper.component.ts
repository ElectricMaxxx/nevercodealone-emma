import { Input, OnInit } from '@angular/core';
import {
  CategoryResource,
  Country,
  Organisation,
  User,
} from '@nevercodealone/data-models';

import { Component } from '@angular/core';
import * as fromStore from '../../store';
import { Router } from '@angular/router';
import {
  AuthenticationService,
  AuthorizationService,
} from '@nevercodealone/shared-functions';
import { Store } from '@ngrx/store';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'nevercodealone-navigation-wrapper',
  templateUrl: './navigation-wrapper.component.html',
  styleUrls: ['./navigation-wrapper.component.scss'],
})
export class NavigationWrapperComponent implements OnInit {
  organisation$ = this.store.select(fromStore.getOrganisationEntity);
  selectedCategory$ = this.store.select(fromStore.getSelectedCategory);
  selectedCountry$ = this.store.select(fromStore.getSelectedCountry);
  user$ = this.authenticationService.user$;
  user: User | null = null;
  selectedCategory: CategoryResource | null = null;
  selectedCountry: Country | null = null;
  organisation: Organisation | null = null;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private authorizationService: AuthorizationService,
    private store: Store<fromStore.AppState>
  ) {
    this.user$.subscribe((x) => (this.user = x));
  }

  ngOnInit(): void {
    this.selectedCategory$.subscribe((cat) => (this.selectedCategory = cat));
    this.selectedCountry$.subscribe(
      (country) => (this.selectedCountry = country)
    );
    this.organisation$.subscribe((org) => (this.organisation = org));
  }

  get title(): string {
    return (
      'EMMA' +
      (this.organisation !== null ? ' - ' + this.organisation.name : '')
    );
  }

  isUser() {
    return this.authorizationService.isUser;
  }

  isEmmaAdmin() {
    return this.authorizationService.isEmmaAdmin;
  }

  isOrganisationAdmin() {
    return this.authorizationService.isOrganisationAdmin;
  }
}
