import { AppComponent } from './app.component';
import { CountrySelectionComponent } from './country/country.selection.component';
import { CountrySelectionItemComponent } from './country/country-selection-item.component';
import { PhraseCollectionComponent } from './phrase-collection.component';
import { PhraseItemComponent } from './phrase-item.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CategorySelectionItemComponent } from './category/category-selection-item.component';
import { CategorySelectionComponent } from './category/category.selection.component';
import { ChatMessageBoardComponent } from './chat-message/chat-message-board.component';
import { AdminComponent } from './admin/admin.component';
import { NavigationWrapperComponent } from './navigation-wrapper/navigation-wrapper.component';

export const components: any[] = [
  AppComponent,
  CountrySelectionComponent,
  CountrySelectionItemComponent,
  CategorySelectionComponent,
  CategorySelectionItemComponent,
  PhraseCollectionComponent,
  PhraseItemComponent,
  LoginComponent,
  HomeComponent,
  ChatMessageBoardComponent,
  AdminComponent,
  NavigationWrapperComponent,
];

export * from './app.component';
export * from './country/country.selection.component';
export * from './country/country-selection-item.component';
export * from './category/category-selection-item.component';
export * from './phrase-collection.component';
export * from './phrase-item.component';
export * from './login/login.component';
export * from './home/home.component';
export * from './category/category.selection.component';
export * from './chat-message/chat-message-board.component';
export * from './admin/admin.component';
export * from './navigation-wrapper/navigation-wrapper.component';
