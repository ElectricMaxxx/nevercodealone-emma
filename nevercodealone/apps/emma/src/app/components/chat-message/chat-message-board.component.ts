import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ChatMessageModel } from '../../models';
import {
  CategoryResource,
  PhraseResource,
  Country,
  NewPhraseResource,
} from '@nevercodealone/data-models';
import { Utils } from '../../services';

type PhraseText = { text: string; phrase: PhraseResource };

@Component({
  selector: 'nevercodealone-chat-message-board',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './chat-message-board.component.html',
  styleUrls: ['./chat-message-board.component.scss'],
})
export class ChatMessageBoardComponent implements OnChanges {
  @Input() phrases: PhraseResource[] = [];
  @Input() messages: ChatMessageModel[] = [];
  @Input() country: Country | null = null;
  @Input() isRight = false;
  @Input() category: CategoryResource | null = null;

  @Output()
  selectPhrase: EventEmitter<PhraseResource> = new EventEmitter<PhraseResource>();
  @Output()
  createNewPhrase: EventEmitter<NewPhraseResource> = new EventEmitter<NewPhraseResource>();
  phraseLabel: PhraseText | null = null;
  createNewForm = false;
  newPhraseLabel = '';
  phraseLabels: PhraseText[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    this.getPhraseLabels();
  }
  onCreateNewPhrase(): void {
    if (null === this.category || null === this.country) {
      return;
    }
    this.createNewPhrase.emit({
      name: this.newPhraseLabel,
      category: this.category,
    });
  }

  onPhraseSelection(phrase: PhraseResource): void {
    this.selectPhrase.emit(phrase);
  }

  getPhraseLabels(): void {
    if (null === this.category || null === this.country) {
      this.phraseLabels = [];
    }

    this.phraseLabels = this.phrases
      .filter((phrase) => {
        return this.category?.id === phrase.category.id;
      })
      .reduce((acc: PhraseText[], phrase: PhraseResource): PhraseText[] => {
        const text = Utils.getTranslationByCountry(phrase, this.country);
        acc.push({ text, phrase });
        return acc;
      }, []);
  }

  toggleCreateNewForm(): void {
    this.createNewForm = !this.createNewForm;
  }
}
