import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChatMessageBoardComponent } from './chat-message-board.component';
import { environment } from './../../../environments/environment';

import { PhraseResource } from '@nevercodealone/data-models';

describe('ChatMessageBoardComponent', () => {
  let component: ChatMessageBoardComponent;
  let fixture: ComponentFixture<ChatMessageBoardComponent>;
  const TestPhrase: PhraseResource = {
    name: 'test phrase',
    id: 1,
    translations: [{ translation: 'test-translation', language_key: 'gb' }],
    category: { id: 1, category: 'test category' },
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ChatMessageBoardComponent],
      }).compileComponents();
    })
  );

  describe('Phrase selection', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ChatMessageBoardComponent);
      component = fixture.componentInstance;
      component.country = environment.baseCountry;
      component.phrases = [TestPhrase];
      fixture.detectChanges();
    });

    it('should render one phrase', () => {
      const HTML: HTMLElement = fixture.nativeElement;
      expect(HTML.querySelector('ul.phrase-selection')?.childElementCount).toBe(
        1
      );
    });
  });

  describe('Base tests', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ChatMessageBoardComponent);
      component = fixture.componentInstance;
      component.country = environment.baseCountry;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });
});
