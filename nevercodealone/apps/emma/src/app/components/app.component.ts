import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Role, User } from '@nevercodealone/data-models';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';
import { AuthenticationService } from '@nevercodealone/shared-functions';

@Component({
  selector: 'nevercodealone-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'EMMA';

  user$ = this.authenticationService.user$;
  currentUser: User | null = null;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private store: Store<fromStore.AppState>
  ) {
    this.authenticationService.user$.subscribe((x) => (this.currentUser = x));
  }

  ngOnInit(): void {
    this.user$.subscribe((user: User | null) => {
      if (null === user) {
        return;
      }

      this.store.dispatch(fromStore.LoadOrganisationAction());
      this.store.dispatch(fromStore.LoadPhrasesAction());
      this.store.dispatch(fromStore.LoadCategoriesAction());
    });
  }

  logout() {
    this.store.dispatch(fromStore.RemoveOrganisationFromStoreAction());
    this.store.dispatch(fromStore.RemovePhrasesFromStoreAction());
    this.store.dispatch(fromStore.RemoveCategoriesFromStoreAction());

    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
