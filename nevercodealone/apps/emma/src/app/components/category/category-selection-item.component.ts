import {
  Component,
  EventEmitter,
  Input,
  Output,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CategoryResource } from '@nevercodealone/data-models';

@Component({
  selector: 'nevercodealone-category-selection-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="sb-box sb-bottom-right" (click)="onSelectCategory()">
      {{ category?.category }}
    </div>
  `,
})
export class CategorySelectionItemComponent {
  @Input() category: CategoryResource | null = null;
  @Output()
  selectCategory: EventEmitter<CategoryResource> = new EventEmitter<CategoryResource>();

  onSelectCategory(): void {
    if (null !== this.category) {
      this.selectCategory.emit(this.category);
    }
  }
}
