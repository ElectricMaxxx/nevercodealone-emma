import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { CategoryResource } from '@nevercodealone/data-models';
import * as fromStore from '../../store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'nevercodealone-category-selection',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="category-component">
      <mat-grid-list cols="2" rowHeight="2:1">
        <mat-grid-tile>
          <div class="sb-list">
            <div class="sb-box sb-box-wide sb-bottom-left">
              Ich bin hier ...
            </div>
          </div>
          <mat-grid-tile-header
            >Suche dir eine Station oder Stelle aus, wo du heute im Einsatz
            bist.
          </mat-grid-tile-header>
        </mat-grid-tile>
        <mat-grid-tile *ngIf="(categories$ | async)?.length">
          <div class="sb-list">
            <nevercodealone-category-selection-item
              [category]="category"
              *ngFor="let category of categories$ | async"
              (selectCategory)="onSelectCategory($event)"
            ></nevercodealone-category-selection-item>
          </div>
        </mat-grid-tile>
      </mat-grid-list>
      <div class="row" *ngIf="!(categories$ | async)?.length">
        <div class="sb-box sb-box-wide sb-bottom-left sb-warning">
          Leider sind noch keine Abteilungen für deine Organisation angelegt
          angelegt.
        </div>
      </div>
    </div>
  `,
})
export class CategorySelectionComponent implements OnInit {
  categories$: Observable<CategoryResource[]> = new Observable();
  selectedCategory$: Observable<CategoryResource | null> = new Observable();

  constructor(
    private router: Router,
    private store: Store<fromStore.AppState>
  ) {}

  onSelectCategory(category: CategoryResource): void {
    this.store.dispatch(fromStore.SelectCategory({ payload: category }));
    this.router.navigate(['country/' + category.id]);
  }

  ngOnInit(): void {
    this.store.dispatch(fromStore.SelectCategory({ payload: null }));
    this.selectedCategory$ = this.store.select(fromStore.getSelectedCategory);
    this.categories$ = this.store.select(fromStore.getAllCategories);
  }
}
