import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  CategoryResource,
  NewPhraseResource,
  PhraseResource,
  Country,
} from '@nevercodealone/data-models';
type Created = {
  [id: number]: { category: CategoryResource; phrases: PhraseResource[] };
};

@Component({
  selector: 'nevercodealone-phrase-collection',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="container" *ngIf="isCountrySelected()">
      <div class="row">
        <div
          class="col-4 phrase-category"
          *ngFor="let categoryWithPhrase of sortedByCategory"
        >
          <h4>{{ categoryWithPhrase.category.category }}</h4>
          <nevercodealone-phrase-item
            [phrase]="phrase"
            [selectedCountry]="selectedCountry"
            *ngFor="let phrase of categoryWithPhrase.phrases"
          ></nevercodealone-phrase-item>
        </div>
      </div>
      <div class="form-container col-md-4">
        <form>
          <div class="form-group" *ngIf="selectedPhrase !== null">
            <label for="category-select">Category</label>
            <ng-select
              id="category-select"
              [items]="categories"
              [multiple]="true"
              bindLabel="category"
              [closeOnSelect]="false"
              bindValue="id"
            >
              <ng-template
                ng-option-tmp
                let-item="item"
                let-item$="item$"
                let-index="index"
              >
                <input id="item-{{ index }}" type="checkbox" />
                {{ item.category }}
              </ng-template>
            </ng-select>
          </div>
          <div class="form-group" *ngIf="selectedPhrase !== null">
            <label for="name">Phrase</label>
            <input
              type="text"
              class="form-control"
              required
              [(ngModel)]="selectedPhrase.name"
              name="phrase"
            />
          </div>
          <button
            type="submit"
            class="btn btn-primary mr-1"
            [disabled]="createNewIsDisabled()"
            (click)="onCreatePhrase()"
          >
            Übersetzen
          </button>
          <button type="reset" class="btn btn-secondary" (click)="resetForm()">
            Reset
          </button>
        </form>
      </div>
    </div>
  `,
})
export class PhraseCollectionComponent implements OnInit, OnChanges {
  @Input() selectedCountry: Country | null = null;
  @Input() phrases: PhraseResource[] = [];
  @Input() categories: CategoryResource[] = [];

  @Output() createPhrase = new EventEmitter<NewPhraseResource>();

  sortedByCategory: {
    category: CategoryResource;
    phrases: PhraseResource[];
  }[] = [];

  selectedPhrase: { name: string; category: [] } | null = null;

  ngOnInit(): void {
    this.sortByCategory();
    this.resetForm();
  }

  resetForm() {
    this.selectedPhrase = {
      name: '',
      category: [],
    };
  }

  createNewIsDisabled(): boolean {
    if (this.selectedPhrase === null) {
      return true;
    }
    return this.selectedPhrase.category === null;
  }

  ngOnChanges(): void {
    this.sortByCategory();
  }

  onCreatePhrase(): void {
    if (this.selectedPhrase === null) {
      return;
    }
    const categoryId = this.selectedPhrase.category.pop();
    const matchedCategories = this.categories.filter(
      (category) => category.id === categoryId
    );
    if (matchedCategories.length === 1) {
      const matchedCategory = matchedCategories.pop() as CategoryResource;
      const phrase = {
        name: this.selectedPhrase.name,
        category: matchedCategory,
      };

      this.createPhrase.emit(phrase);
      this.resetForm();
    }

    this.selectedPhrase.category = [];
    return;
  }

  public isCountrySelected(): boolean {
    return this.selectedCountry !== null && this.selectedCountry.name !== null;
  }

  private sortByCategory(): void {
    if (null === this.phrases || null === this.categories) {
      return;
    }

    const entities = this.phrases.reduce((list: Created, item): Created => {
      const categoryId = item.category.id;
      if (
        null !== categoryId ||
        typeof list[categoryId].phrases === 'undefined'
      ) {
        if (!list[categoryId]) {
          list = {
            ...list,
            [categoryId]: { category: item.category, phrases: [] },
          };
        }
        list[categoryId].phrases.push(item);
      }
      return list;
    }, {});
    this.sortedByCategory = Object.keys(entities).map(
      (id) => entities[parseInt(id, 10)]
    );
  }
}
