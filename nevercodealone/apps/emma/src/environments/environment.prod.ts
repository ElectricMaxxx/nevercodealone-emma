export const environment = {
  production: true,
  apiUrl: 'https://api.emma.nevercodealone.de/api',
  backendUrl: 'https://api.emma.nevercodealone.de',
  baseCountry: {name: 'Deutschland', language_key: 'de'},
};
