export const environment = {
  production: false,
  apiUrl: 'http://0.0.0.0:8000/api',
  backendUrl: 'http://0.0.0.0:8000',
  baseCountry: {name: 'Deutschland', language_key: 'de'},
};
