import { Component } from '@angular/core';

@Component({
  selector: 'nevercodealone-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'test-emma';
}
