module.exports = {
  projects: [
    '<rootDir>/apps/emma',
    '<rootDir>/apps/test-emma',
    '<rootDir>/libs/admin-ui',
    '<rootDir>/libs/data-models',
    '<rootDir>/libs/shared-functions',
    '<rootDir>/libs/core-module',
  ],
};
