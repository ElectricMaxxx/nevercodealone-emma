# Emmma die App für die Duisburger Tafel

Hier geht es um ein Rewrite und um das neue Konzept der "Emma für die Duisburger Tafel" Aktuell steht es noch in der Entwicklung. Die aktuell möglichen Features werden mit Angular und einem Symfony Backend umgesetzt.

Todos:

Man findet es aufgeschlüsselt wenn man sich die [Isues]('/issues') anschaut.

## Usage


### Einmaliges - Vorbereitung

Kopiere die Vorlage für die Environment-Variable im Backend Folder an:

```shell
cd emma-backend/app/src
cp .env.dist .env
```

und passe in `.env` einige secrets an. Ein großteil der Variablen können in der Docker-Lösung ausgelassen werden, da sie im Container
gesetzt werden.

**Abhängigkeiten Laden Frontend**:

```shell
cd nevercodealone
npm install
```
**Abhängigkeiten Laden Backend**:

```shell
cd emma-backend/app/src
composer install
```
**Datenbank bauen oder befüllen**:

```shell
cd emma-backend/app/src
composer run load:database #Run only on first call to init DB
composer run load:fixtures
```

**Keys für den JWT Token bauen**:

```shell
cd emma-backend/app/src
./bin/build_keys.sh
```

### Laufen lassen der Applikationen

**Frontend**:

```shell
cd nevercodealone
npm run start
```

**Backend**:

```shell
cd emma-backend/app/src
composer run start
```

## Changelog

* *2021-12-03*: Simplify usage and UI (speech bubbles)
* *2021-07-17*: Reindroduce docker-compose setup for dev environment, Refresh base images

### 0.0.8

* *2021-06-04*: Several updates of dependencies 
* *2021-06-04*: Rename DTO to Model
* *2021-06-04*: Enable lots of typing in php
* *2021-06-04*: Fixes to get it running.

### 0.0.3

* *2019-11-17*: Add login mechanic to frontend, use fixtures for firs user/organisation pair.
* *2019-11-17*: Use JWT authendication to build a user token based login for backend application, frontend should not get any value now anymore.

### 0.0.2

* *2019-11-15*: API application läuft und erlaubt GET auf resourcen wie `Category`, `Translation`, `Phrase`, `Country`.
* *2019-11-111*: Docker Setup läuft wieder und erlaubt über `docker-compose -f docker-compose.dev.yml up -d` das Hochfahren aller Services.

### 0.0.1

* *2019-10-30*: Wieder zusammen führen der App Teile, Update auf Angular8
