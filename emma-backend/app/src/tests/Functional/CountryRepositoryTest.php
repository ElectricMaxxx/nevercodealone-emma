<?php

namespace App\Tests\Functional\Repository;

use App\Entity\Country;
use App\Entity\User;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CountryRepositoryTest extends AbstractRepositoryTest
{
    public function testGetCountriesByUser()
    {
        $users = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'johndoe']);

        $categories = $this->entityManager->getRepository(Country::class)->findCountriesInOneOrganisationByUser($users);
        $this->assertCount(4, $categories);
    }

    public function testCreateCountryByUser()
    {
        $users = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'johndoe']);

        $country = new Country();
        $country->setLanguageKey('ab');
        $country->setName('Testing');
        $countryResult = $this->entityManager->getRepository(Country::class)->addCountryToUsersOrganisation($users, $country);

        $this->assertInstanceOf(Countryr::class, $countryResult);
    }
}
