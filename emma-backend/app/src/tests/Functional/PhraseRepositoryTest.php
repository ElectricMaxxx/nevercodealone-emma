<?php

namespace App\Tests\Functional\Repository;

use App\Entity\Country;
use App\Entity\Phrase;
use App\Entity\User;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class PhraseRepositoryTest extends AbstractRepositoryTest
{
    public function testGetPhrasesByUser()
    {
        $users = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'johndoe']);

        $countries = $this->entityManager->getRepository(Phrase::class)->findPhrasesInOneOrganisationByUser($users);

        $this->assertCount(6, $countries);
    }
}
