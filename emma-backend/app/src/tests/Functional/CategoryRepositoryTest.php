<?php

namespace App\Tests\Functional\Repository;

use App\Entity\Category;
use App\Entity\Country;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CategoryRepositoryTest extends AbstractRepositoryTest
{
    public function testGetCategoriesByUser()
    {
        /** @var User $users */
        $users = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'johndoe']);

        $categories = $this->entityManager->getRepository(Category::class)->findCategoriesInOrganisationByUser($users);
        $this->assertCount(2, $categories);
    }
}
