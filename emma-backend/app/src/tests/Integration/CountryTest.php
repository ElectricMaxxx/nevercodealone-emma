<?php

namespace App\Tests\Integration;

class CountryTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
        $this->setUpApiUser(['user_role' => 'ROLE_USER']);
    }

    public function testGetCollection(): void
    {
        static::$client->request(
            'GET',
            '/api/countries',
            [],
            [],
            $this->headers
        );
        $response = static::$client->getResponse();
        $content = json_decode($response->getContent());

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertCount(4, $content);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testCreateCountryReturnsCreated(): void
    {
        static::$client = static::createClient();
        static::$client->request(
            'POST',
            '/api/countries',
            [],
            [],
            $this->headers,
            json_encode(['name' => 'doesnotwork', 'languageKey' => 'de'])
        );
        $this->assertResponseStatusCodeSame(201);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testUpdateOwnCountryReturnsOK(): void
    {
        static::$client = static::createClient();
        $body = json_encode(['name' => 'other name', 'languageKey' => 'de']);
        static::$client->request(
            'PUT',
            '/api/countries',
            [],
            [],
            $this->headers,
            $body
        );
        $this->assertResponseStatusCodeSame(405);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testUpdateOtherCountryReturnsNotAuthorized(): void
    {
        static::$client = static::createClient();
        static::$client->request(
            'PUT',
            '/api/countries/1',
            [],
            [],
            $this->headers,
            json_encode(['name' => 'other name', 'languageKey' => 'de'])
        );
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testDeleteCountry(): void
    {
        static::$client = static::createClient();
        static::$client->request(
            'DELETE',
            '/api/countries/1',
            [],
            [],
            $this->headers,
        );

        $this->assertResponseStatusCodeSame(404);
    }
}
