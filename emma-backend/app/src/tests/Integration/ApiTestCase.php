<?php
declare(strict_types=1);

namespace App\Tests\Integration;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

/**
 * Base class for functional API tests.
 *
 * @experimental
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
abstract class ApiTestCase extends WebTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database content to a known state before each test
    use RefreshDatabaseTrait;

    /** @var string */
    protected $authToken;

    /** @var array */
    protected $headers;

    /** @var  Application $application */
    protected static $application;

    /** @var  Client $client */
    protected static $client;

    /** @var  EntityManager $entityManager */
    protected $entityManager;

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }

    protected  function setUp(): void
    {
        parent::setUp();
        self::getApplication();
        static::$client = static::createClient();

        self::runCommand('doctrine:database:drop --force');
        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:create');
        self::runCommand('doctrine:fixtures:load --append --no-interaction');
        $this->entityManager = static::$client->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$client) {
            self::$client = self::createClient();
        }
        if (null === self::$kernel) {
            self::$kernel = self::$client->getKernel();
            static::$kernel->boot();
        }
        if (null === self::$application) {
            self::$application = new Application(self::$kernel);
            self::$application->setAutoExit(false);
        }


        return self::$application;
    }

    protected static function createKernel(array $options = [])
    {

        return parent::createKernel($options);
    }

    protected function findBy(string $resourceClass, array $criteria): ?string
    {
        if (null === static::$container) {
            throw new \RuntimeException(sprintf('The container is not available. You must call "bootKernel()" or "createClient()" before calling "%s".', __METHOD__));
        }
        $objectManager = static::$container->get('doctrine')->getManagerForClass($resourceClass);
        $item = $objectManager->getRepository($resourceClass)->findOneBy($criteria);
        if (null === $item) {
            return null;
        }

        return $item;
    }

    /**
     */
    protected function setUpApiUser(array $options): void
    {
        if (isset($options['user_role'])) {
            /** @var User $user */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => 'johndoe']);
            $user->setRoles([$options['user_role']]);
            try {
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            } catch (ORMException $e) {
                $this->fail('Problems to change user roles: '.$e->getMessage());
            }
        }
        try {
            $jsonBody = json_encode(['username' => 'johndoe', 'password' => 'test'], JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->fail('Body should be valid json: '. $e->getMessage());
        }
        static::$client->request(
            'POST',
            '/login_check',
            ['headers' => [
                'content-type' => 'application/json',
                'accept' => 'application'
            ]],
            [],
            [],
            $jsonBody
        );
        $content = [];
        try {
            $content = json_decode(static::$client->getResponse()->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            $this->assertTrue(false, 'The content of a login request must be able to be parsed as json.');
        }

        $this->assertArrayHasKey('token', $content);
        $this->assertNotNull($content['token']);
        $this->authToken = $content['token'];
        $this->headers = [
            'HTTP_AUTHORIZATION' => 'Bearer ' . $this->authToken,
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json'
        ];
    }


    /**
     * @return string[]
     */
    protected function assertSuccessfulJsonResponse(): array
    {
        $response = static::$client->getResponse();
        $content = [];
        try {
            $content = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->fail('The response must be a valid json.');
        }
        self::assertResponseIsSuccessful();
        self::assertResponseHeaderSame('content-type', 'application/json');

        return $content;
    }
}
