<?php # Berater/Consulting unterschiedlich

namespace App\Tests\Integration;


class OrganisationTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
        $this->setUpApiUser(['user_role' => 'ROLE_USER']);
    }

    public function testGetUsersOrganisation(): void
    {
         static::$client->request(
            'GET',
            '/api/organisation',
            [],
            [],
            $this->headers
            );
        $response =  static::$client->getResponse();
        $content = json_decode($response->getContent());

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertSame(1, $content->id);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testCreateOrganisationFails(): void
    {
         static::$client->request(
            'POST',
            '/api/organisations',
            [],
            [],
            $this->headers,
            json_encode(['name' => 'doesnotwork'])
        );
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testUpdateOrganisation(): void
    {
         static::$client->request(
            'PUT',
            '/api/organisations/1',
            [],
            [],
            $this->headers,
            json_encode(['name' => 'other name'])
        );
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testDeleteOrganisation(): void
    {
         static::$client->request(
            'DELETE',
            '/api/organisations/1',
            [],
            [],
            $this->headers
        );

        $this->assertResponseStatusCodeSame(404);
    }
}
