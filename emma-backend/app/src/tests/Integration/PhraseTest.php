<?php # Berater/Consulting unterschiedlich

namespace App\Tests\Integration;

class PhraseTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
        $this->setUpApiUser(['user_role' => 'ROLE_USER']);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testGetCollectionFailsOnResourceDirectly(): void
    {
        static::$client->request(
            'GET',
            '/api/phrases',
            [],
            [],
            ['Accept' => 'application/json']
        );

        $this->assertResponseStatusCodeSame(401);
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }
    public function testGetCollection(): void
    {
         static::$client->request(
            'GET',
            '/api/phrases',
            [],
            [],
            $this->headers,
        );
        $response =  static::$client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertResponseIsSuccessful();


        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertCount(6, $content);
    }
    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testCreatePhrase(): void
    {
         static::$client->request(
            'POST',
            '/api/phrases',
                 [],
                 [],
                 $this->headers,
                 json_encode(['name' => 'doesnotwork', 'category' => 1])
        );
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testUpdatePhrase(): void
    {
         static::$client->request(
             'PUT',
             '/api/phrases/1',
             [],
             [],
             $this->headers,
             json_encode(['name' => 'other name', 'category' => 1])
         );
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testDeletePhrase(): void
    {
        static::$client->request(
            'DELETE',
                 '/api/phrases/1',
                 [],
                 [],
                 $this->headers
        );

        $this->assertResponseStatusCodeSame(404);
    }
}
