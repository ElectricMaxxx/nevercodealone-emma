<?php

namespace App\Tests\Integration\Admin;

use App\Tests\Integration\ApiTestCase;
use JsonException;

class UserApiControllerTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
    }

    public function testGetCollectionAsAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/users',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();
        $this->assertCount(2, $content);
    }

    public function getDataForUserCreation(): array
    {
        return [
            'An organisation admin can create an user' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_USER', true],
            'An organisation admin can create an organisation admin' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_ORGANISATION_ADMIN', true],
            'An organisation admin cannot create an emma admin' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_ADMIN', false],
            'An user can not create an emma admin' => ['ROLE_USER', 'ROLE_ADMIN', false],
            'An user can not create an organisation admin' => ['ROLE_USER', 'ROLE_ORGANISATION_ADMIN', false],
            'An user can not create an user' => ['ROLE_USER', 'ROLE_USER', false],
            'An emma admin can create an user' => ['ROLE_ADMIN', 'ROLE_USER', true],
            'An emma admin can create an organisation admin' => ['ROLE_ADMIN', 'ROLE_ORGANISATION_ADMIN', true],
            'An emma admin cannot create an emma admin' => ['ROLE_ADMIN', 'ROLE_ADMIN', true],
        ];
    }

    public function getDataForUserPatch(): array
    {
        return [
            'An organisation admin can set role to user' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_USER', true],
            'An organisation admin can set role to organisation admin' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_ORGANISATION_ADMIN', true],
            'An organisation admin can not set role emma admin' => ['ROLE_ORGANISATION_ADMIN', 'ROLE_ADMIN', false],
            'An user can not set role an emma admin' => ['ROLE_USER', 'ROLE_ADMIN', false],
            'An user can not set role an organisation admin' => ['ROLE_USER', 'ROLE_ORGANISATION_ADMIN', false],
            'An user can not set role an user' => ['ROLE_USER', 'ROLE_USER', false],
            'An emma admin can set role an user' => ['ROLE_ADMIN', 'ROLE_USER', true],
            'An emma admin can set role an organisation admin' => ['ROLE_ADMIN', 'ROLE_ORGANISATION_ADMIN', true],
            'An emma admin cannot set role an emma admin' => ['ROLE_ADMIN', 'ROLE_ADMIN', true],
        ];
    }

    public function testGetCollectionAsOrganisationAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ORGANISATION_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/users',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();
        $this->assertCount(1, $content);
    }


    /**
     * @param $clientUserRole
     * @param $userRole
     * @param $success
     *
     * @dataProvider getDataForUserPatch
     */
    public function testUserPatch($clientUserRole, $userRole, $success): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ADMIN']);
        static::$client->request(
            'POST',
            '/api/admin/users',
            [],
            [],
            $this->headers,
            json_encode([
                'user_name' => 'testuser',
                'role' => 'ROLE_USER',
                'first_name' => 'Test',
                'last_name' => 'User',
                'email' => 'test@example.com',
                'organisation_id' => 1,
            ], JSON_THROW_ON_ERROR)
        );
        $newUser = $this->assertSuccessfulJsonResponse();


        $this->setUpApiUser(['user_role' => $clientUserRole]);
        static::$client->request(
            'PUT',
            '/api/admin/users/'.$newUser['id'],
            [],
            [],
            $this->headers,
            json_encode([
                'user_name' => 'testuser',
                'role' => $userRole,
                'first_name' => 'Test',
                'last_name' => 'User',
                'email' => 'test@example.com',
                'organisation_id' => 1,
            ], JSON_THROW_ON_ERROR)
        );
        if ($success) {
            $content = $this->assertSuccessfulJsonResponse();
            $this->assertEquals([
                'id' => 3,
                'user_name' => 'testuser',
                'role' => $userRole,
                'first_name' => 'Test',
                'last_name' => 'User',
                'email' => 'test@example.com',
                'organisation_id' => 1,
                'organisation_name' => 'Tafel Duisburg',
                'is_active' => true,
            ], $content);
        } else {
            self::assertResponseStatusCodeSame(403);
        }
    }
}
