<?php

namespace Integration\Admin;

use App\Controller\Admin\CategoriesApiController;
use App\Tests\Integration\ApiTestCase;
use PHPUnit\Framework\TestCase;

class CategoriesApiControllerTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
    }

    public function testGetCollectionAsAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/categories',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();

        $this->assertCount(2, $content);
    }

    public function testGetCollectionAsOrganisationAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ORGANISATION_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/categories',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();
        $this->assertCount(2, $content);
    }

    public function getDataForCategoryCreation(): array
    {
        return [
            'An organisation admin can create a category in it`s own organisation' => ['ROLE_ORGANISATION_ADMIN', 1, true],
            'An organisation admin can not create a category in an other organisation' => ['ROLE_ORGANISATION_ADMIN', 2, 403],
            'An organisation admin can not create a category with no organisation' => ['ROLE_ORGANISATION_ADMIN', false, 400],
            'An admin can create a category in a given organisation' => ['ROLE_ADMIN', 1, true],
            'An admin can not create a category without a organisation' => ['ROLE_ADMIN', false, 400],
        ];
    }

    /**
     * @param $asRole
     * @param $forOrganisationId
     * @param $isSuccessful
     * @throws \JsonException
     *
     * @dataProvider getDataForCategoryCreation
     */
    public function testCreateCategory($asRole, $forOrganisationId, $isSuccessful): void
    {
        $this->setUpApiUser(['user_role' => $asRole]);

        $body = ['category' => 'Tresen'];
        if (is_numeric($forOrganisationId)) {
            $body['organisation_id'] = $forOrganisationId;
        }

        static::$client->request(
            'POST',
            '/api/admin/categories',
            [],
            [],
            $this->headers,
            json_encode($body, JSON_THROW_ON_ERROR)
        );
        if ($isSuccessful === true) {
            $content = $this->assertSuccessfulJsonResponse();
        } else {
            self::assertResponseStatusCodeSame($isSuccessful, static::$client->getResponse()->getStatusCode());
        }
    }
}
