<?php

namespace Integration\Admin;

use App\Controller\Admin\CategoriesApiController;
use App\Tests\Integration\ApiTestCase;
use PHPUnit\Framework\TestCase;

class OrganisationsApiControllerTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
    }

    public function testGetCollectionAsAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/organisations',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();

        $this->assertCount(2, $content);
    }

    public function testGetCollectionAsOrganisationAdminUser(): void
    {
        $this->setUpApiUser(['user_role' => 'ROLE_ORGANISATION_ADMIN']);
        static::$client->request(
            'GET',
            '/api/admin/organisations',
            [],
            [],
            $this->headers
        );
        $content = $this->assertSuccessfulJsonResponse();
        $this->assertCount(1, $content);
    }
}
