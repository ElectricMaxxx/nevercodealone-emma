<?php # Berater/Consulting unterschiedlich

namespace App\Tests\Integration;

use App\Entity\Category;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class CategoryTest extends ApiTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        static::$client = static::createClient();
        $this->setUpApiUser(['user_role' => 'ROLE_USER']);
    }

    public function testGetCollection(): void
    {
         static::$client->request(
            'GET',
            '/api/categories',
            [],
            [],
            $this->headers
        );
        $response =  static::$client->getResponse();
        $content = json_decode($response->getContent());
        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertCount(2, $content);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testCreateCategoryFails(): void
    {
        $body = ['name' => 'doesnotwork'];
         static::$client->request(
            'POST',
            '/api/categories',
            [],
            [],
            $this->headers,
            json_encode($body)
    );
        $this->assertResponseStatusCodeSame(405);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testUpdateCategory(): void
    {
        $body = ['name' => 'other name'];
        static::$client->request(
            'PUT',
            '/api/categories/1',
            [],
            [],
            $this->headers,
            json_encode($body)
        );
        $this->assertResponseStatusCodeSame(404);
    }

    /**
     * @ExpectedException MethodNotAllowedHttpException:
     */
    public function testDeleteCategory(): void
    {
        static::$client->request(
            'DELETE',
            '/api/categories/1',
            [],
            [],
            $this->headers
        );

        $this->assertResponseStatusCodeSame(404);
    }
}
