#!/bin/bash

APP_BASEDIR_SRC=${APP_BASEDIR_SRC-"$PWD"}
uid=$(whoami)
SYSTEM_APPUSER_NAME=${SYSTEM_APPUSER_NAME-"${uid}"}
JWT_SECRET_KEY=${JWT_SECRET_KEY-"config/jwt/private.key"}
JWT_PUBLIC_KEY=${JWT_PUBLIC_KEY-"config/jwt/public.key"}
JWT_PASSPHRASE=${JWT_PASSPHRASE-"df9885f0fe5dbd78fd12841073acf3b3"}

mkdir -p ${APP_BASEDIR_SRC}/config/jwt
openssl genpkey -out ${APP_BASEDIR_SRC}/${JWT_SECRET_KEY} -pass pass:${JWT_PASSPHRASE} -aes256 -algorithm rsa
openssl pkey -in ${APP_BASEDIR_SRC}/${JWT_SECRET_KEY} -passin pass:${JWT_PASSPHRASE} -out ${APP_BASEDIR_SRC}/${JWT_PUBLIC_KEY}  -pubout

chmod -R 775 ${APP_BASEDIR_SRC}/config/jwt/
chown -R ${SYSTEM_APPUSER_NAME}:${SYSTEM_APPUSER_NAME} ${APP_BASEDIR_SRC}/config/jwt/