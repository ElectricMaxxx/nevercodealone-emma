<?php

namespace App\Form;


use App\Model\UserModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('userName')
            ->add('email')
            ->add('isActive', CheckboxType::class, [])
            ->add('organisationId', NumberType::class)
            ->add('role', ChoiceType::class, ['choices' => [
                'ROLE_ADMIN',
                'ROLE_ORGANISATION_ADMIN',
                'ROLE_USER'
            ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'             => UserModel::class,
                'allow_extra_fields' => true,
                'csrf_protection'    => false,
            ]
        );
    }
}