<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Services\BuildHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadOrganisations extends Fixture
{
    private array $categories = [];
    private array $countries = [
        ['langKey' => 'de', 'name' => 'Deutschland'],
        ['langKey' => 'gb', 'name' => 'Groß Britanien'],
        ['langKey' => 'fr', 'name' => 'Frankreich'],
        ['langKey' => 'ru', 'name' => 'Rußland'],
        ['langKey' => 'pl'  , 'name' => 'Polen'],
    ];

    private array $phrases = [
        [
            "name" => "Kommst du aus Duisburg?",
            "category" =>  "Ausgabe",
            "translations" => [
                ["translation" => "You come from Duisburg?", "languageKey" => "gb"],
                ["translation" => "Êtes-vous de Duisburg?", "languageKey" => "fr"]
            ],
        ],
        [
            "name" => "Wie heißt du",
            "category" => "Ausgabe",
            "translations" => [
                [
                    "translation" => "What is your name?",
                    "languageKey" => "gb"
                ],
                [
                    "translation" => "Quesque tu ...?",
                    "languageKey" => "fr"
                ],
            ],
        ],
        [
            "name" => "Wie alt bist du?",
            "category" => "Anmeldung",
            "translations" => [
                ["translation" => "How old are you?", "languageKey" => "gb"],
                ["translation" => "Quel âge avez-vous?", "languageKey" => "fr"],
            ],
        ],
        [
            "name" => "Wo kommst du her?",
            "category" => "Anmeldung",
            "translations" => [
                ["translation" => "Where are you from?", "languageKey" => "gb"],
                ["translation" => "D'où venez-vous?", "languageKey" => "fr"]
            ],
        ],
        [
            "name" => "Ich bin neu hier",
            "category" => "Ausgabe",
            "translations" => [
                ["translation" => "I'm new here", "languageKey" => "gb"],
                ["translation" => "Je suis nouveau ici", "languageKey" => "fr"],
            ],
        ],
        [
            "name" => "Ich habe Hunger",
            "category" => "Ausgabe",
            "translations" => [
                ["translation" => "I am hungy", "languageKey" => "gb"],
                ["translation" => "J'ai faim", "languageKey" => "fr"]
            ],
        ],
    ];

    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $countries = [];
        foreach ($this->countries as $countryData) {
            $country = BuildHelper::buildCountry($countryData['langKey'], $countryData['name']);
            $manager->persist($country);

            $countries[$countryData['langKey']] = $country;
        }

        $organisation = BuildHelper::buildOrganisation('Tafel Duisburg');
        $organisation = BuildHelper::addUser($this->encoder, 'johndoe', 'test', $organisation, 'John', 'Doe', 'john.doe@example.com', ['ROLE_ADMIN']);
        $organisation = BuildHelper::addCountries($organisation, array_slice($countries, 1,4));
        $organisations[0] = $organisation;
        $manager->persist($organisation);

        $organisation = BuildHelper::buildOrganisation('Tafel Franken');
        $organisation = BuildHelper::addUser($this->encoder, 'jamesdoe', 'test', $organisation, 'James', 'Doe', 'james.doe@example.com', ['ROLE_ORGANISATION_ADMIN']);
        $organisation = BuildHelper::addCountries($organisation, array_slice($countries, 1,3));
        $organisations[1] = $organisation;
        $manager->persist($organisation);

        foreach ($this->phrases as $phraseData) {
            $phrase = BuildHelper::buildPhraseOnOrganisation(
                $phraseData['name'],
                $this->buildOrReuseCategory($phraseData['category']),
                $phraseData['translations'],
                $organisations[0]
            );
            $manager->persist($phrase);

        }

        foreach ($this->phrases as $phraseData) {
            $phrase = BuildHelper::buildPhraseOnOrganisation(
                $phraseData['name'],
                $this->buildOrReuseCategory($phraseData['category']),
                $phraseData['translations'],
                $organisations[1]
            );
            $manager->persist($phrase);
        }

        $manager->flush();
    }

    private function buildOrReuseCategory(string $name): Category
    {
        if (!isset($this->categories[$name])) {
            $this->categories[$name] = BuildHelper::buildCategory($name);
        }

        return $this->categories[$name];
    }
}
