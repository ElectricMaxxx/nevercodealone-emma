<?php

namespace App\Services;

use App\Entity\Phrase;
use App\Entity\Translation;
use App\Exception\TranslationException;
use Google\Cloud\Translate\V2\TranslateClient;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class Translator
{
    private TranslateClient $translateClient;

    public function __construct(TranslateClient $translateClient)
    {
        $this->translateClient = $translateClient;
    }

    public function translate(Phrase $phrase, string $targetLang): Phrase
    {
        try {
            $result = $this->translateClient->translate($phrase->getName(), ['target' => $targetLang]);
            if (null === $result || !is_array($result) || !isset($result['text'])) {
                throw new \RuntimeException('not found');
            }
        } catch (\Throwable $exception) {
            $result['text'] = 'not found';
            throw new TranslationException('Problems during translation: '. $exception->getMessage(), $exception->getCode(), $exception);
        }

        $translation = new Translation();
        $translation->setLanguageKey($targetLang);
        $translation->setTranslation($result['text']);

        $phrase->addTranslation($translation);
        $translation->setPhrase($phrase);

        return $phrase;
    }
}
