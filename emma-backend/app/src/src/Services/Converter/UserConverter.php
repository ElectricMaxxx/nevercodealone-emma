<?php

namespace App\Services\Converter;

use App\Entity\Organisation;
use App\Entity\User;
use App\Model\UserModel;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class UserConverter
{
    /**
     * @param User $user
     *
     * @return UserModel
     */
    public static function convertUserToUserDTO(User $user): UserModel
    {
        return UserModel::fromEntity($user);
    }

    /**
     * @param User[] $users
     *
     * @return UserModel[]
     */
    public static function convertUsersToUserDTOs(array $users): array
    {
        $userDTOs = [];
        foreach ($users as $user) {
            $userDTOs[] = static::convertUserToUserDTO($user);
        }

        return $userDTOs;
    }

    public static function convertDTOToEntity(UserModel $newUser, ?Organisation $organisation = null, ?User $existingUser = null): User
    {
        if (!isset($existingUser)) {
            $existingUser = new User();
        }
        if (isset($organisation)) {
            $existingUser->setOrganisation($organisation);
        }

        $existingUser->setEmail($newUser->getEmail());
        $existingUser->setFirstName($newUser->getFirstName());
        $existingUser->setLastName($newUser->getLastName());
        $existingUser->setUsername($newUser->getUserName());
        $existingUser->setRoles([$newUser->getRole()]);

        return $existingUser;
    }
}
