<?php

namespace App\Services\Converter;

use App\Entity\Category;
use App\Model\CategoryModel;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CategoryConverter
{
    /**
     * @param Category $category
     *
     * @return CategoryModel
     */
    public static function convertCategoryToCategoryDTO(Category $category): CategoryModel
    {
        $categoryDTO = new CategoryModel();
        $categoryDTO->id = $category->getId();
        $categoryDTO->description = $category->getDescription();
        $categoryDTO->category = $category->getCategory();

        return $categoryDTO;
    }

    /**
     * @param Category[] $categories
     *
     * @return CategoryModel[]
     */
    public static function convertCategoriesToCategoryDTOs(array $categories): array
    {
        $categoryDTOs = [];
        foreach ($categories as $category) {
            $categoryDTOs[] = static::convertCategoryToCategoryDTO($category);
        }

        return $categoryDTOs;
    }
}
