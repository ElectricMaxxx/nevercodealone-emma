<?php

namespace App\Services\Converter;

use App\Entity\Translation;
use App\Model\PhraseTranslationModel;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class TranslationConverter
{
    public static function convertTranslationToTranslationDTO(Translation $translation): PhraseTranslationModel
    {
        $dto = new PhraseTranslationModel();
        $dto->id = $translation->getId();
        $dto->languageKey = $translation->getLanguageKey();
        $dto->translation = $translation->getTranslation();

        return  $dto;
    }

    /**
     * @param Translation[] $translations
     *
     * @return Translation[]
     */
    public static function convertTranslationsToTranslationDTOs(array $translations): array
    {
        $translationDTOs = [];
        foreach ($translations as $translation) {
            $translationDTOs[] = static::convertTranslationToTranslationDTO($translation);
        }

        return $translationDTOs;
    }
}
