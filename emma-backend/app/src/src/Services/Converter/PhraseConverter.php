<?php

namespace App\Services\Converter;

use App\Entity\Phrase;
use App\Model\PhraseModel;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class PhraseConverter
{
    public static function convertPhraseToPhraseDTO(Phrase $phrase): PhraseModel
    {
        $phraseDTO = new PhraseModel();
        $phraseDTO->id = $phrase->getId();
        $phraseDTO->translations = TranslationConverter::convertTranslationsToTranslationDTOs($phrase->getTranslations()->toArray());
        $phraseDTO->category = CategoryConverter::convertCategoryToCategoryDTO($phrase->getCategory());
        $phraseDTO->name = $phrase->getName();

        return  $phraseDTO;

    }

    /**
     * @param Phrase[] $phrases
     *
     * @return PhraseModel[]
     */
    public static function convertPhrasesToPhraseDTOs(array $phrases): array
    {
        $phraseDTOs = [];
        foreach ($phrases as $phrase) {
            $phraseDTOs[] = static::convertPhraseToPhraseDTO($phrase);
        }

        return $phraseDTOs;
    }
}
