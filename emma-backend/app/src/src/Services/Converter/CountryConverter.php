<?php

namespace App\Services\Converter;

use App\Entity\Country;
use App\Model\CountryModel;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CountryConverter
{
    /**
     * @param Country $country
     *
     * @return CountryModel
     */
    public static function convertCountryToCountryDTO(Country $country): CountryModel
    {
        $countryDTO = new CountryModel();
        $countryDTO->id = $country->getId();
        $countryDTO->name = $country->getName();
        $countryDTO->languageKey = $country->getLanguageKey();

        return  $countryDTO;
    }

    /**
     * @param Country[] $countries
     *
     * @return CountryModel[]
     */
    public static function convertCountriesToCountryDTOs(array $countries): array
    {
        $countryDTOs = [];
        foreach ($countries as $country) {
            $countryDTOs[] = static::convertCountryToCountryDTO($country);
        }

        return $countryDTOs;
    }
}
