<?php

namespace App\Services\Converter;

use App\Model\OrganisationModel;
use App\Entity\Organisation;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class OrganisationConverter
{
    /**
     * @param Organisation $organisation
     *
     * @return OrganisationModel
     */
    public static function convertOrganisationToOrganisationModel(Organisation $organisation): OrganisationModel
    {
        $categories = [];
        foreach ($organisation->getPhrases() as $phrase) {
            $categories[] = $phrase->getCategory();
        }
        $dto = new OrganisationModel();
        $dto->id = $organisation->getId();
        $dto->name = $organisation->getName();
        $dto->countries = CountryConverter::convertCountriesToCountryDTOs($organisation->getCountries()->toArray());
        $dto->phrases = PhraseConverter::convertPhrasesToPhraseDTOs($organisation->getPhrases()->toArray());
        $dto->categories = CategoryConverter::convertCategoriesToCategoryDTOs($categories);
        $dto->users = UserConverter::convertUsersToUserDTOs($organisation->getUsers()->toArray());

        return  $dto;
    }

    /**
     * @param Organisation[] $organisations
     *
     * @return OrganisationModel[]
     */
    public static function convertOrganisationsToOrganisationModels(array $organisations): array
    {
        $OrganisationModels = [];
        foreach ($organisations as $organisation) {
            $OrganisationModels[] = static::convertOrganisationToOrganisationModel($organisation);
        }

        return $OrganisationModels;
    }
}
