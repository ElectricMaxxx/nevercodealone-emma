<?php

namespace App\Services;

use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Organisation;
use App\Entity\Phrase;
use App\Entity\Translation;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BuildHelper
{
    public static function buildOrganisation($name): Organisation
    {
        $organisation = new Organisation();
        $organisation->setName($name);

        return $organisation;
    }

    public static function addUser(
        UserPasswordEncoderInterface $encoder,
        string $name,
        string $password,
        Organisation $organisation,
        $firstName = null,
        $lastName = null,
        $email = null,
        array $roles = []
    ): Organisation {
        $user = new User();
        $user->setUsername($name);
        $user->setPassword($encoder->encodePassword($user, $password));
        if (null !== $firstName) {
            $user->setFirstName($firstName);
        }
        if (null !== $lastName) {
            $user->setLastName($lastName);
        }
        if (null !== $email) {
            $user->setEmail($email);
        }
        foreach ($roles as $role) {
            $user->addRole($role);
        }
        $organisation->addUser($user);
        $user->setOrganisation($organisation);

        return $organisation;
    }

    public static function buildCategory(string $name): Category
    {
        $category = new Category();
        $category->setCategory($name);

        return $category;
    }

    public static function buildPhraseOnOrganisation(string $name, Category $category, array $translations, Organisation $organisation): Phrase
    {
        $phrase = new Phrase();
        $phrase->setName($name);
        $phrase->setCategory($category);
        $phrase->setOrganisation($organisation);
        foreach($translations as $translation) {
            $translation = (self::buildTranslation($translation['languageKey'], $translation['translation']));
            $phrase->addTranslation($translation);
            $translation->setPhrase($phrase);
        }
        $translation = self::buildTranslation('de', $name);
        $phrase->addTranslation($translation);
        $translation->setPhrase($phrase);

        return $phrase;
    }

    public static function buildTranslation(string $langKey, string $translationString): Translation
    {
        $translation = new Translation();
        $translation->setLanguageKey($langKey);
        $translation->setTranslation($translationString);

        return $translation;
    }


    /**
     * @param Country[] $countries
     */
    public static function addCountries(Organisation $organisation, array $countries): Organisation
    {
        foreach ($countries as $country) {
            $organisation->addCountry($country);
            $country->addOrganisation($organisation);
        }

        return $organisation;
    }

    public static function buildCountry(string $langKey, string $name): Country
    {
        $country = new Country();
        $country->setLanguageKey($langKey);
        $country->setName($name);

        return $country;
    }
}