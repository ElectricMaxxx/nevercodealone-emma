<?php

namespace App\Factory\Services;

use App\Services\Translator;
use Google\Cloud\Translate\V2\TranslateClient;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class TranslatorFactory
{
    public function build(string $apiKey): TranslateClient
    {
        return new TranslateClient([$apiKey]);
    }
}
