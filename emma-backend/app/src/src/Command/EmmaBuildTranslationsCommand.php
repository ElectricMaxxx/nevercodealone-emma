<?php


namespace App\Command;


use App\Entity\Country;
use App\Entity\Translation;
use App\Repository\CountryRepository;
use App\Repository\OrganisationRepository;
use App\Services\Translator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Organisation;
use App\Entity\Phrase;
use App\Repository\PhraseRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;

class EmmaBuildTranslationsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'emma:build:translations';
    private ManagerRegistry $doctrine;
    private Translator $translator;

    public function __construct(ManagerRegistry $doctrine, Translator $translator)
    {
        $this->doctrine = $doctrine;
        $this->translator = $translator;
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->setDescription('Fetches all phrases with missing translations and builds it with the help of google tranlator');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->doctrine->getManagerForClass(Phrase::class);
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->doctrine->getRepository(Organisation::class);
        /** @var Organisation[] $organisations */
        $organisations = $organisationRepository->findAll();
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->doctrine->getRepository(Country::class);
        /** @var PhraseRepository $phraseRepository */
        $phraseRepository = $this->doctrine->getRepository(Phrase::class);
        $output->writeln('Fetch all Organisations. Found: '. count($organisations));
        foreach ($organisations as $organisation) {;
            $phrases = $phraseRepository->findPhrasesInOrganisation($organisation);
            $output->writeln('Fetch all Phrases of ' . $organisation->getName().'. Found: '. count($phrases));
            $countries = $countryRepository->findCountriesInOneOrganisation($organisation);
            $countCountries = count($countries);
            $mappedLanguageKeys = array_map(function (Country $country) {
                return $country->getLanguageKey();
            }, $countries);
            foreach ($phrases as $phrase) {
                $translations = $phrase->getTranslations()->toArray();
                $countTranslations = count($translations);
                $output->writeln('Phrase "' . $phrase->getName().'" got translations: '. $countTranslations . '/' . $countCountries);
                if ($countCountries !== $countTranslations) {
                    $mappedLanguageKeysInTranslation = array_map(function (Translation $translation) {
                        return $translation->getLanguageKey();
                    }, $translations);
                    $diff = array_diff_key($mappedLanguageKeys, $mappedLanguageKeysInTranslation);
                    foreach ($diff as $languageKey) {
                        try {
                            $output->writeln('Build translation for Phrase "' . $phrase->getName().'" in language:  "'. $languageKey . '".');
                            $phrase = $this->translator->translate($phrase, $languageKey);
                            $em->persist($phrase);
                            foreach ($phrase->getTranslations() as $translation) {
                                var_dump([$translation->getLanguageKey(), $translation->getTranslation()]);
                                $em->persist($translation);
                            }
                        } catch (\Throwable $e) {
                            $output->writeln('<error>Problem to build translation: '.$e->getMessage().'</error>');
                        }
                        $output->writeln('Translations created! ' . count($phrase->getTranslations()) . '/' . $countCountries);
                    }
                }

            }
        }

        $em->flush();
        return 0;
    }
}