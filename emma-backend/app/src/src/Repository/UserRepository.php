<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserRepository extends EntityRepository
{
    /**
     * @param Organisation $organisation
     * @return User[]
     */
    public function findUsesInOrganisationOfAnUser(User $user): array
    {
        $qb = $this->getEntityManager()->getRepository(Organisation::class)->createQueryBuilder('organisation');
        $organisations = $qb->select('organisation', 'users')
            ->join(User::class, 'users')
            ->where('users.id = :userId')
            ->setParameter('userId', $user->getId())
            ->getQuery()
            ->execute();
        if (0 === count($organisations)) {
            throw new BadRequestHttpException('User is in no admin');
        }
        if (1 > count($organisations)) {
            throw new BadRequestHttpException('User is in more then one organisation');
        }
        $organisation = array_shift($organisations);

        return $organisation->getUsers()->toArray();
    }
}