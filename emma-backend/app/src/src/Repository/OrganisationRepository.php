<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Organisation;
use App\Entity\Phrase;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query\Expr\Join;

class OrganisationRepository extends EntityRepository
{
    public function findOneOrganisationByUser(User $user):? Organisation
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $organisations =  $qb
            ->select('organisation', 'phrases', 'category')
            ->distinct(true)
            ->from(Organisation::class, 'organisation')
            ->innerJoin('organisation.users', 'users', Join::WITH, $qb->expr()->eq('users.id', ':userId'))
            ->leftJoin('organisation.countries', 'countries')
            ->innerJoin('organisation.phrases', 'phrases')
            ->innerJoin('phrases.category', 'category')
            ->orderBy('organisation.name')
            ->setParameter('userId', $user->getId())
            ->getQuery()->execute();

        if (count($organisations) === 1) {
            return array_shift($organisations);
        }

        return  null;
    }

    /**
     * @param User $user
     *
     * @return Country[]
     */
    public function findCountriesInOneOrganisationByUser(User $user): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        /** @var Organisation[]|PersistentCollection $organisations */
        $organisations =  $qb
            ->select('organisation', 'countries')
            ->distinct(true)
            ->from(Organisation::class, 'organisation')
            ->innerJoin('organisation.users', 'users', Join::WITH, $qb->expr()->eq('users.id', ':userId'))
            ->innerJoin('organisation.countries', 'countries')
            ->setParameter('userId', $user->getId())
            ->getQuery()->execute();

        $countries = [];
        foreach ($organisations as $organisation) {
            foreach ($organisation->getCountries() as $country) {
                $countries[] = $country;
            }
        }

        return $countries;
    }

    /**
     * @param User $user
     *
     * @return Phrase[]
     */
    public function findPhrasesInOneOrganisationByUser(User $user): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        /** @var PersistentCollection|Organisation[] $organisations */
        $organisations =  $qb
            ->select('organisation', 'phrases', 'category')
            ->distinct(true)
            ->from(Organisation::class, 'organisation')
            ->innerJoin('organisation.users', 'users', Join::WITH, $qb->expr()->eq('users.id', ':userId'))
            ->innerJoin('organisation.phrases', 'phrases')
            ->innerJoin('phrases.category', 'category')
            ->orderBy('organisation.name')
            ->setParameter('userId', $user->getId())
            ->getQuery()->execute();
        $phrases = [];

        foreach ($organisations as $organisation) {
            $phrases = array_merge($phrases, $organisation->getPhrases()->toArray());
        }

        return  $phrases;
    }

    /**
     * @param User $user
     *
     * @return Category[]
     */
    public function findCategoriesInOrganisationByUser(User $user): array
    {
        $phrases = $this->findPhrasesInOneOrganisationByUser($user);
        $categories = [];
        foreach ($phrases as $phrase) {
            $category = $phrase->getCategory();
            $categories[$category->getId()] = $category;
        }

        return  $categories;
    }

    public function findAllCountriesOfOrganisation(Organisation $organisation): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        /** @var Organisation[]|PersistentCollection $organisations */
        $organisations =  $qb
            ->select('organisation', 'countries')
            ->distinct(true)
            ->from(Organisation::class, 'organisation')
            ->innerJoin('organisation.countries', 'countries')
            ->where('organisation.id = :organisationId')
            ->setParameter('organisationId', $organisation->getId())
            ->getQuery()->execute();

        $countries = [];
        foreach ($organisations as $organisation) {
            foreach ($organisation->getCountries() as $country) {
                $countries[] = $country;
            }
        }

        return $countries;
    }

}

