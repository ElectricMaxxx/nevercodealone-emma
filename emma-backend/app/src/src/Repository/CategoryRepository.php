<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Organisation;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CategoryRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return Category[]
     */
    public function findCategoriesInOrganisationByUser(User $user): array
    {
        /** @var OrganisationRepository $repository */
        $repository = $this->getEntityManager()->getRepository(Organisation::class);

        return  $repository->findCategoriesInOrganisationByUser($user);
    }

    /**
     * @return Category []
     */
    public function findAllCategoriesWithPhrasesInAllOrganisations(): array
    {
        $categories = $this->createQueryBuilder('category')
            ->select('category', 'phrase', 'organisation', 'translation')
            ->join('category.phrases', 'phrase')
            ->join('phrase.organisation', 'organisation')
            ->join('phrase.translations', 'translation')
            ->getQuery()->execute();

        return $categories;
    }
}
