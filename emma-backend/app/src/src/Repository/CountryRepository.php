<?php

namespace App\Repository;

use App\Entity\Country;
use App\Entity\Organisation;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CountryRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return Country[]
     */
    public function findCountriesInOneOrganisationByUser(User $user): array
    {
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->getEntityManager()->getRepository(Organisation::class);

        return $organisationRepository->findCountriesInOneOrganisationByUser($user);
    }

    public function addCountryToUsersOrganisation(User $user, Country $country): Country
    {
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->getEntityManager()->getRepository(Organisation::class);

        $organisation = $organisationRepository->findOneOrganisationByUser($user);
        if (!$organisation instanceof Organisation) {
            throw new \RuntimeException('No organisation for user found');
        }

        $organisation->addCountry($country);
        $country->addOrganisation($organisation);
        try {
            $this->getEntityManager()->persist($organisation);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new \RuntimeException('Problems to persist an organisation');
        }
        return $country;
    }

    /**
     * @param Organisation $organisation
     * @return Country[]
     */
    public function findCountriesInOneOrganisation(Organisation $organisation): array
    {
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->getEntityManager()->getRepository(Organisation::class);

        return $organisationRepository->findAllCountriesOfOrganisation($organisation);
    }
}
