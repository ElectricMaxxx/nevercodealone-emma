<?php

namespace App\Repository;

use App\Entity\Organisation;
use App\Entity\Phrase;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class PhraseRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return Phrase[]
     */
    public function findPhrasesInOneOrganisationByUser(User $user): array
    {
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->getEntityManager()->getRepository(Organisation::class);

        return $organisationRepository->findPhrasesInOneOrganisationByUser($user);
    }

    public function addPhraseByUsersOrganisation(User $user, Phrase $phrase):? Phrase
    {
        /** @var OrganisationRepository $organisationRepository */
        $organisationRepository = $this->getEntityManager()->getRepository(Organisation::class);

        $organisation = $organisationRepository->findOneOrganisationByUser($user);
        if (!$organisation instanceof Organisation) {
            throw new \RuntimeException('No organisation for user found');
        }

        $organisation->addPhase($phrase);
        $phrase->setOrganisation($organisation);
        try {
            $this->getEntityManager()->persist($phrase);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new \RuntimeException('Problems to persist.', $e->getCode(), $e);
        }

        return $phrase;
    }

    /**
     * @param Organisation $organisation
     * @return Phrase[]
     */
    public function findPhrasesInOrganisation(Organisation $organisation): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $organisations =  $qb
            ->select('organisation', 'phrase', 'translations')
            ->distinct(true)
            ->from(Phrase::class, 'phrase')
            ->join('phrase.organisation', 'organisation')
            ->join('phrase.translations', 'translations')
            ->where('organisation.id = :organisationId')
            ->setParameter('organisationId', $organisation->getId())
            ->getQuery()->execute();

        return $organisations;

    }
}
