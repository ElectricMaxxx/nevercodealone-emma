<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206021137 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_EA42BEA6F92F3E70');
        $this->addSql('DROP INDEX IDX_EA42BEA69E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__country_organisation AS SELECT country_id, organisation_id FROM country_organisation');
        $this->addSql('DROP TABLE country_organisation');
        $this->addSql('CREATE TABLE country_organisation (country_id INTEGER NOT NULL, organisation_id INTEGER NOT NULL, PRIMARY KEY(country_id, organisation_id), CONSTRAINT FK_EA42BEA6F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_EA42BEA69E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO country_organisation (country_id, organisation_id) SELECT country_id, organisation_id FROM __temp__country_organisation');
        $this->addSql('DROP TABLE __temp__country_organisation');
        $this->addSql('CREATE INDEX IDX_EA42BEA6F92F3E70 ON country_organisation (country_id)');
        $this->addSql('CREATE INDEX IDX_EA42BEA69E6B1585 ON country_organisation (organisation_id)');
        $this->addSql('DROP INDEX IDX_A24BE60C12469DE2');
        $this->addSql('DROP INDEX IDX_A24BE60C9E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__phrase AS SELECT id, category_id, organisation_id, name FROM phrase');
        $this->addSql('DROP TABLE phrase');
        $this->addSql('CREATE TABLE phrase (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, organisation_id INTEGER DEFAULT NULL, name CLOB NOT NULL COLLATE BINARY, CONSTRAINT FK_A24BE60C12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_A24BE60C9E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO phrase (id, category_id, organisation_id, name) SELECT id, category_id, organisation_id, name FROM __temp__phrase');
        $this->addSql('DROP TABLE __temp__phrase');
        $this->addSql('CREATE INDEX IDX_A24BE60C12469DE2 ON phrase (category_id)');
        $this->addSql('CREATE INDEX IDX_A24BE60C9E6B1585 ON phrase (organisation_id)');
        $this->addSql('DROP INDEX IDX_B469456F8671F084');
        $this->addSql('CREATE TEMPORARY TABLE __temp__translation AS SELECT id, phrase_id, translation, language_key FROM translation');
        $this->addSql('DROP TABLE translation');
        $this->addSql('CREATE TABLE translation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, phrase_id INTEGER DEFAULT NULL, translation CLOB NOT NULL COLLATE BINARY, language_key CLOB NOT NULL COLLATE BINARY, CONSTRAINT FK_B469456F8671F084 FOREIGN KEY (phrase_id) REFERENCES phrase (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO translation (id, phrase_id, translation, language_key) SELECT id, phrase_id, translation, language_key FROM __temp__translation');
        $this->addSql('DROP TABLE __temp__translation');
        $this->addSql('CREATE INDEX IDX_B469456F8671F084 ON translation (phrase_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('DROP INDEX IDX_8D93D6499E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, organisation_id INTEGER DEFAULT NULL, first_name CLOB DEFAULT NULL COLLATE BINARY, last_name CLOB DEFAULT NULL COLLATE BINARY, email CLOB DEFAULT NULL COLLATE BINARY, username VARCHAR(25) NOT NULL COLLATE BINARY, password VARCHAR(500) DEFAULT NULL COLLATE BINARY, is_active BOOLEAN NOT NULL, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:array)
        , salt VARCHAR(500) NOT NULL COLLATE BINARY, CONSTRAINT FK_8D93D6499E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user (id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt) SELECT id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE INDEX IDX_8D93D6499E6B1585 ON user (organisation_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_EA42BEA6F92F3E70');
        $this->addSql('DROP INDEX IDX_EA42BEA69E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__country_organisation AS SELECT country_id, organisation_id FROM country_organisation');
        $this->addSql('DROP TABLE country_organisation');
        $this->addSql('CREATE TABLE country_organisation (country_id INTEGER NOT NULL, organisation_id INTEGER NOT NULL, PRIMARY KEY(country_id, organisation_id))');
        $this->addSql('INSERT INTO country_organisation (country_id, organisation_id) SELECT country_id, organisation_id FROM __temp__country_organisation');
        $this->addSql('DROP TABLE __temp__country_organisation');
        $this->addSql('CREATE INDEX IDX_EA42BEA6F92F3E70 ON country_organisation (country_id)');
        $this->addSql('CREATE INDEX IDX_EA42BEA69E6B1585 ON country_organisation (organisation_id)');
        $this->addSql('DROP INDEX IDX_A24BE60C12469DE2');
        $this->addSql('DROP INDEX IDX_A24BE60C9E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__phrase AS SELECT id, category_id, organisation_id, name FROM phrase');
        $this->addSql('DROP TABLE phrase');
        $this->addSql('CREATE TABLE phrase (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, category_id INTEGER DEFAULT NULL, organisation_id INTEGER DEFAULT NULL, name CLOB NOT NULL)');
        $this->addSql('INSERT INTO phrase (id, category_id, organisation_id, name) SELECT id, category_id, organisation_id, name FROM __temp__phrase');
        $this->addSql('DROP TABLE __temp__phrase');
        $this->addSql('CREATE INDEX IDX_A24BE60C12469DE2 ON phrase (category_id)');
        $this->addSql('CREATE INDEX IDX_A24BE60C9E6B1585 ON phrase (organisation_id)');
        $this->addSql('DROP INDEX IDX_B469456F8671F084');
        $this->addSql('CREATE TEMPORARY TABLE __temp__translation AS SELECT id, phrase_id, translation, language_key FROM translation');
        $this->addSql('DROP TABLE translation');
        $this->addSql('CREATE TABLE translation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, phrase_id INTEGER DEFAULT NULL, translation CLOB NOT NULL, language_key CLOB NOT NULL)');
        $this->addSql('INSERT INTO translation (id, phrase_id, translation, language_key) SELECT id, phrase_id, translation, language_key FROM __temp__translation');
        $this->addSql('DROP TABLE __temp__translation');
        $this->addSql('CREATE INDEX IDX_B469456F8671F084 ON translation (phrase_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('DROP INDEX IDX_8D93D6499E6B1585');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, organisation_id INTEGER DEFAULT NULL, first_name CLOB DEFAULT NULL, last_name CLOB DEFAULT NULL, email CLOB DEFAULT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(500) DEFAULT NULL, is_active BOOLEAN NOT NULL, roles CLOB NOT NULL --(DC2Type:array)
        , salt VARCHAR(500) NOT NULL)');
        $this->addSql('INSERT INTO user (id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt) SELECT id, organisation_id, first_name, last_name, email, username, password, is_active, roles, salt FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE INDEX IDX_8D93D6499E6B1585 ON user (organisation_id)');
    }
}
