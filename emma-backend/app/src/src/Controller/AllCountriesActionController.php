<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class AllCountriesActionController extends  AbstractFOSRestController
{
    /**
     * @var string[]
     */
    private array $allCountries;

    /**
     * AllCountriesActionController constructor.
     *
     * @param string[] $allCountries
e     */
    public function __construct(array $allCountries)
    {
        $this->allCountries = $allCountries;
    }

    /**
     * @Route(name="all_countries", path="/api/all-countries")
     */
    public function index(): View
    {
        return $this->view($this->allCountries);
    }
}
