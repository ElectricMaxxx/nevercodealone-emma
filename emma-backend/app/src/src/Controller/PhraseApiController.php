<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Phrase;
use App\Entity\User;
use App\Exception\BadRequestDataException;
use App\Exception\TranslationException;
use App\Services\Converter\PhraseConverter;
use App\Services\Translator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @RouteResource("Phrase")
 */
class PhraseApiController extends AbstractFOSRestController
{
    private Translator $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function cgetAction(): View
    {
        $user = $this->getUser();
        if (!$user) {
            throw new UnauthorizedHttpException('emma-api');
        }

        $data = $this->getDoctrine()->getRepository(Phrase::class)->findPhrasesInOneOrganisationByUser($user);

        return $this->view(PhraseConverter::convertPhrasesToPhraseDTOs($data), Response::HTTP_OK);
    }

    public function postAction(Request $request): View
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user) {
            throw new UnauthorizedHttpException('emma-api');
        }

        $name = $request->get('name');
        $categoryId = $request->get('category');
        $translations = $request->get('translations', []);
        if (empty($name)) {
            throw new BadRequestDataException('no phrase name given');
        }
        if (empty($categoryId)) {
            throw new BadRequestDataException('No category given');
        }
        /** @var Category $category */
        $category = $this->getDoctrine()->getRepository(Category::class)->find($categoryId);
        if (null === $category) {
            throw new BadRequestDataException('NO category found');
        }
        $phrase = new Phrase();
        $phrase->setName($name);
        $phrase->setCategory($category);
        foreach ($translations as $translation) {
            try {
                $phrase = $this->translator->translate($phrase, $translation['languageKey'] );
            } catch (TranslationException $exception) {

            }
        }
        /** @var Phrase $phrase */
        $phrase = $this->getDoctrine()
            ->getRepository(Phrase::class)
            ->addPhraseByUsersOrganisation($user, $phrase);

        return $this->view(PhraseConverter::convertPhraseToPhraseDTO($phrase), Response::HTTP_CREATED);
    }
}
