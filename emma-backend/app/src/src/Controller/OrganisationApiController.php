<?php

namespace App\Controller;

use App\Entity\Organisation;
use App\Entity\User;
use App\Services\Converter\OrganisationConverter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@gmx.de>
 *
 * @RouteResource("Organisation")
 */
class OrganisationApiController extends AbstractFOSRestController
{
    public function getAction(): View
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('emma-api');
        }
        $data = $this->getDoctrine()->getRepository(Organisation::class)->findOneOrganisationByUser($user);
        if (!$data instanceof Organisation) {
            throw new AccessDeniedHttpException();
        }

        return $this->view(OrganisationConverter::convertOrganisationToOrganisationModel($data), Response::HTTP_OK);
    }
}
