<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\User;
use App\Exception\BadRequestDataException;
use App\Services\Converter\CountryConverter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 *
 * @RouteResource("Country")
 */
class CountryApiController extends AbstractFOSRestController
{
    /**
     * @var string[]
     */
    private array $allCountries;

    /**
     * AllCountriesActionController constructor.
     *
     * @param string[] $allCountries
     */
    public function __construct(array $allCountries)
    {
        $this->allCountries = $allCountries;
    }

    public function cgetAction(): View
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('emma-api');
        }

        $data = $this->getDoctrine()->getRepository(Country::class)->findCountriesInOneOrganisationByUser($user);

        return $this->view(CountryConverter::convertCountriesToCountryDTOs($data), Response::HTTP_OK);
    }

    public function postAction(Request $request): View
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('emma-api');
        }

        $langKey = $request->get('languageKey');
        $name = $request->get('name');

        $selectedCountries = array_filter($this->allCountries, function ($country) use ($name, $langKey) {
            return $country['language_key'] === $langKey;
        });

        if (1 !== count($selectedCountries)) {
            throw new BadRequestDataException('NO matching country found.');
        }
        $country = new Country();
        $country->setLanguageKey($langKey);
        $country->setName($name);
        try {
            $data = $this->getDoctrine()->getRepository(Country::class)->addCountryToUsersOrganisation($user, $country);
        } catch (\RuntimeException $exception) {
            throw new BadRequestDataException($exception->getMessage());
        }

        return $this->view(CountryConverter::convertCountryToCountryDTO($data), Response::HTTP_CREATED);
    }
}
