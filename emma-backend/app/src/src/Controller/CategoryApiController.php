<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\User;
use App\Services\Converter\CategoryConverter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;
/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class CategoryApiController extends AbstractFOSRestController
{
    /**
     * @return array
     *
     * @Annotations\View()
     */
    public function getCategoriesAction(): array
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('emma-api');
        }
        $data = $this->getDoctrine()->getRepository(Category::class)->findCategoriesInOrganisationByUser($user);

        return CategoryConverter::convertCategoriesToCategoryDTOs($data);
    }
}
