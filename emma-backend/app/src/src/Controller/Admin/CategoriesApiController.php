<?php

namespace App\Controller\Admin;

use AdminCategoryModel;
use App\Entity\Category;
use App\Entity\Organisation;
use App\Entity\Phrase;
use App\Services\BuildHelper;
use App\Services\Converter\CategoryConverter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CategoriesApiController extends AbstractAdminApiController
{
    /**
     * @return array
     *
     * @Annotations\View()
     * @Annotations\Get(name="_admin")
     */
    public function getCategoriesAction(): array
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }
        $categories = [];
        if ($this->isAdmin()) {
            $categories = $this->getDoctrine()->getRepository(Category::class)->findAllCategoriesWithPhrasesInAllOrganisations();
        } elseif ($this->isOrganisationAdmin()) {
            $categories = $this->getDoctrine()->getRepository(Category::class)->findCategoriesInOrganisationByUser($this->getUser());
        }

        return array_map(static function(Category $category) {
            return AdminCategoryModel::fromEntity($category);
        }, $categories);
    }

    public function postCategoryAction(Request $request): view
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }
        $categoryName = $request->get('category', null);
        $organisationId = $request->get('organisationId', null);

        if (null === $categoryName) {
            throw new BadRequestHttpException('No categoryName found to create');
        }

        if (null === $organisationId) {
            throw new BadRequestHttpException('No organisation id found.');
        }
        $organisation = null;
        if ($this->isOrganisationAdmin()) {
            $organisation = $this->getUser()->getOrganisation();
            if (!$organisation instanceof Organisation || $organisationId !== $organisation->getId()) {
                throw new AccessDeniedHttpException('Not your own organisation');
            }
        } elseif ($this->isAdmin()){
            $organisation = $this->getDoctrine()->getRepository(Organisation::class)->findOneBy(['id' => $organisationId]);
        }

        if (!$organisation instanceof Organisation) {
            throw new BadRequestHttpException('No organisation found to create a new category for');
        }

        $existingCategory = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['category' => $categoryName]);
        $category = $existingCategory instanceof Category ? $existingCategory : BuildHelper::buildCategory($categoryName);
        $phrase = BuildHelper::buildPhraseOnOrganisation('Erster Text', $category, [], $organisation);

        $em = $this->getDoctrine()->getManagerForClass(Phrase::class);
        if(null === $em) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'No entity manager found');
        }
        $em->persist($phrase);
        $em->flush();


        return $this->view(CategoryConverter::convertCategoryToCategoryDTO($phrase->getCategory()));
    }
}