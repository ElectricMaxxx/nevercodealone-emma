<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Organisation;
use App\Services\Converter\OrganisationConverter;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrganisationApiController extends AbstractAdminApiController
{
    /**
     * @return array
     *
     * @Annotations\View()
     * @Annotations\Get(name="_admin")
     */
    public function getOrganisationsAction(): array
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }
        $organisations = $this->getDoctrine()->getRepository(Organisation::class)->findAll();
        if ($this->isOrganisationAdmin()) {
            $ownOrganisationId = $this->getUser()->getOrganisation()->getId();
           $organisations = array_filter($organisations, static function ($organisation) use($ownOrganisationId) {
               return $organisation->getId() === $ownOrganisationId;
           });
        }

        return OrganisationConverter::convertOrganisationsToOrganisationModels($organisations);
    }

}