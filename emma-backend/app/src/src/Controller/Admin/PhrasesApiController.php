<?php

namespace App\Controller\Admin;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PhrasesApiController extends AbstractAdminApiController
{
    /**
     * @return View
     *
     * @Annotations\View()
     * @Annotations\Get(name="_admin")
     */
    public function getPhrasesAction(): View
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }
    }
}