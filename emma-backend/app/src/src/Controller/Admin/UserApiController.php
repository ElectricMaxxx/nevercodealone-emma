<?php

namespace App\Controller\Admin;

use App\Entity\Organisation;
use App\Entity\User;
use App\Form\AdminUserFormType;
use App\Model\UserModel;
use App\Services\Converter\UserConverter;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations;

class UserApiController extends AbstractAdminApiController
{
    /**
     * @return View
     * @Annotations\View()
     * @Annotations\Get(name="_admin")
     */
    public function getUsersAction(): View
    {
        if ($this->isAdmin()) {
            $data = $this->getDoctrine()->getRepository(User::class)->findAll();
        } elseif($this->isOrganisationAdmin()) {
            $data = $this->getDoctrine()->getRepository(User::class)->findUsesInOrganisationOfAnUser($this->getUser());
        } else {
            throw new NotFoundHttpException('Not found');
        }

        return $this->view(UserConverter::convertUsersToUserDTOs($data), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return View
     * @Annotations\View()
     * @Annotations\Post(name="_admin")
     */
    public function postUserAction(Request $request): View
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }

        $form = $this->createForm(AdminUserFormType::class, UserModel::createEmpty());

        $form->submit($request->request->all());
        if (false === $form->isValid()) {
            return $this->view($form);
        }

        /** @var UserModel $newUser */
        $newUser = $form->getData();
        if (!($this->isAdmin()) && 'ROLE_ADMIN' === $newUser->getRole()) {
            throw new AccessDeniedHttpException('Not allowed to create an emma admin.');
        }

        $organisation = null;
        if (!($this->isAdmin())) {
            // as an admin you would be allowed to select a organisation for a  user
            $organisation = $this->getDoctrine()->getRepository(Organisation::class)->findOneBy(['id' => $this->getUser()->getOrganisation()->getId()]);
        }  else {
            // as an organisation admin you would be allowed to add a new user to your own organisation only
            $organisation = $this->getDoctrine()->getRepository(Organisation::class)->findOneBy(['id' => $newUser->getOrganisationId()]);
        }
        if (!$organisation instanceof Organisation) {
            throw new BadRequestHttpException('No organisation found');
        }

        $userEntity = UserConverter::convertDTOToEntity($newUser, $organisation);
        $em = $this->getDoctrine()->getManagerForClass(User::class);
        if(null === $em) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'No entity manager found');
        }
        $em->persist($userEntity);
        $em->flush();

        return $this->view(UserConverter::convertUserToUserDTO($userEntity));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View
     * @Annotations\View()
     * @Annotations\Put(name="_admin")
     */
    public function putUserAction(Request $request, int $id): View
    {
        if (!($this->isOrganisationAdmin()) && !($this->isAdmin())) {
            throw new AccessDeniedHttpException('emma-api');
        }

        $existingUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);
        if (!$existingUser instanceof User) {
            throw new NotFoundHttpException();
        }
        if ($this->isOrganisationAdmin() && $existingUser->getOrganisation()->getId() !== $this->getUser()->getOrganisation()->getId()) {
            throw new AccessDeniedHttpException('Organisation admin can not manage user of other organisations');
        }

        $form = $this->createForm(AdminUserFormType::class, UserModel::fromEntity($existingUser));
        $form->submit($request->request->all());

        if (false === $form->isValid()) {
            return $this->view($form);
        }

        /** @var UserModel $newUser */
        $newUser = $form->getData();
        if (!($this->isAdmin()) && 'ROLE_ADMIN' === $newUser->getRole()) {
            throw new AccessDeniedHttpException('Not allowed to create an emma admin.');
        }
        if (!($this->isAdmin()) && $newUser->getOrganisationId() !== $this->getUser()->getOrganisation()->getId()) {
            throw new AccessDeniedHttpException('Not allowed to change organisation.');
        }

        $organisation = null;
        if (!($this->isAdmin())) {
            // as an admin you would be allowed to select a organisation for a  user
            $organisation = $this->getDoctrine()->getRepository(Organisation::class)->findOneBy(['id' => $this->getUser()->getOrganisation()->getId()]);

            if (!$organisation instanceof Organisation) {
                throw new BadRequestHttpException('No organisation found');
            }
        }


        $userEntity = UserConverter::convertDTOToEntity($newUser, $organisation, $existingUser);
        $em = $this->getDoctrine()->getManagerForClass(User::class);
        if(null === $em) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'No entity manager found');
        }
        $em->persist($userEntity);
        $em->flush();

        return $this->view(UserConverter::convertUserToUserDTO($userEntity));
    }
}