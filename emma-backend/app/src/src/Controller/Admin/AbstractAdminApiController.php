<?php

namespace App\Controller\Admin;

use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

abstract class AbstractAdminApiController extends AbstractFOSRestController
{
    protected function isAdmin(): bool
    {
        $user = $this->getUser();
        return $this->isGranted('ROLE_ADMIN', $user);
    }

    protected function isOrganisationAdmin(): bool
    {
        $user = $this->getUser();
        return $this->isGranted('ROLE_ORGANISATION_ADMIN', $user);
    }

    protected function getUser(): User
    {
        $user = parent::getUser();
        if (!$user instanceof User) {
            throw new UnauthorizedHttpException('emma-api');
        }
        return $user;
    }

}