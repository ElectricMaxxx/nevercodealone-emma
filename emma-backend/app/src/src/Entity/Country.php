<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@gmx.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $languageKey;

    /**
     * @var Organisation[]/ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Organisation", inversedBy="countries", cascade={"persist"})
     */
    private $organisations;

    public function __construct()
    {
        $this->organisations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLanguageKey(): string
    {
        return $this->languageKey;
    }

    /**
     * @param string $languageKey
     */
    public function setLanguageKey(string $languageKey): void
    {
        $this->languageKey = $languageKey;
    }

    /**
     * @return Organisation[]/ArrayCollection
     */
    public function getOrganisations()
    {
        return $this->organisations;
    }

    /**
     * @param Organisation[]/ArrayCollection
     */
    public function setOrganisations($organisations): self
    {
        $this->organisations = $organisations;

        return $this;
    }

    public function addOrganisation(Organisation $organisation): self
    {
        $this->organisations->add($organisation);

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): self
    {
        $this->organisations->removeElement($organisation);

        return $this;
    }
}
