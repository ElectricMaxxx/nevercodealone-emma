<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@gmx.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable, JWTUserInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $firstName;
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $lastName;
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $email;
    /**
     * @var string
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    /**
     * @var string
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $password;
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var Organisation
     * @ORM\ManyToOne(targetEntity="Organisation", inversedBy="users", cascade={"persist"})
     */
    private $organisation;
    /**
     * @var string[]
     *
     * @ORM\Column(type="array")
     */
    private $roles;
    /**
     * @var string
     * @ORM\Column(type="string", length=500)
     */
    private $salt;

    public function __construct()
    {
        $this->isActive = true;
        $this->salt = md5(uniqid('', true));
        $this->roles = ['ROLE_USER'];
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail():? string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLastName():? string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName():? string
    {
        return $this->firstName;
    }
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getId():? int
    {
        return $this->id;
    }

    public function getOrganisation(): Organisation
    {
        return $this->organisation;
    }

    public function setOrganisation(Organisation $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param string[] $roles
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(string $role): void
    {
        $this->roles[] = $role;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        [
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->firstName,
            $this->lastName,
            $this->salt
        ] = unserialize($serialized, array('allowed_classes' => false));
    }

    public static function createFromPayload($username, array $payload)
    {
        $entity = new self();
        $entity->setUsername($username);
        $entity->setRoles($payload['roles']);
    }
}
