<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@gmx.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\PhraseRepository")
 */
class Phrase
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="phrases", cascade={"persist"})
     */
    private $category;

    /**
     * @var Translation[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Translation", mappedBy="phrase", cascade={"persist"})
     */
    private $translations;

    /**
     * @var Organisation
     * @ORM\ManyToOne(targetEntity="Organisation", inversedBy="phrases")
     */
    private $organisation;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getId():? int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Translation[]|ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param Translation[]|ArrayCollection $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }

    function addTranslation(Translation $translation): self
    {
        $this->translations->add($translation);

        return $this;
    }

    public function removeTranslation(Translation $translation): self
    {
        $this->translations->removeElement($translation);

        return $this;
    }

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function setOrganisation(Organisation $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }
}
