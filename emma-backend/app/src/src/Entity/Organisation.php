<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@gmx.de>
 *
 * @ORM\Entity(repositoryClass="App\Repository\OrganisationRepository")
 */
class Organisation
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @var User[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="User", mappedBy="organisation", cascade={"persist"})
     */
    private $users;

    /**
     * @var Country[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Country", mappedBy="organisations", cascade={"persist"})
     */
    private $countries;

    /**
     *
     * @var Phrase[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Phrase", mappedBy="organisation", cascade={"persist"})
     */
    private $phrases;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->countries = new ArrayCollection();
        $this->phrases = new ArrayCollection();
    }

    /**
     * Get the value of users
     *
     * @return  User[]|ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set the value of users
     *
     * @param  User[]|ArrayCollection  $users
     *
     * @return  self
     */
    public function setUsers($users): self
    {
        $this->users = $users;

        return $this;
    }

    public function addUser(User $user): self
    {
        $this->users->add($user);

        return $this;
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    /**
     * Get the value of name
     *
     * @return  string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @param  string  $name
     *
     * @return  self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getId():? int
    {
        return $this->id;
    }

    /**
     * @return Country[]|ArrayCollection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set the value of countries
     *
     * @param  Country[]|ArrayCollection  $countries
     *
     * @return  self
     */
    public function setCountries($countries): self
    {
        $this->countries = $countries;

        return $this;
    }

    public function addCountry(Country $country): self
    {
        $this->countries->add($country);

        return $this;
    }

    public function removeCountry(Country $country): self
    {
        $this->countries->removeElement($country);

        return $this;
    }

    /**
     * Get undocumented variable
     *
     * @return  Phrase[]|ArrayCollection
     */
    public function getPhrases()
    {
        return $this->phrases;
    }

    /**
     * Set undocumented variable
     *
     * @param  Phrase[]|ArrayCollection  $phrases
     *
     * @return  self
     */
    public function setPhrases($phrases): self
    {
        $this->phrases = $phrases;

        return $this;
    }

    public function addPhase(Phrase $phrase)
    {
        $this->phrases->add($phrase);

        return $this;
    }

    public function removePhase(Phrase $phrase): self
    {
        $this->phrases->removeElement($phrase);

        return $this;
    }
}
