<?php

namespace App\Model;

use App\Entity\Translation;
use Webmozart\Assert\Assert;

class TranslationResponse {

    private string $message;

    private string $key;

    private function __construct(string $message, string $key)
    {
        $this->message = $message;
        $this->key = $key;
    }
    public static function fromEntity(Translation $translation): self
    {
        return new self($translation->getTranslation(), $translation->getLanguageKey());
    }
    public static function fromTranslationResponse(array $data): self
    {
        Assert::string($data['message']);
        Assert::string($data['key']);

        return new self($data['message'], $data['key']);
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the value of key
     */ 
    public function getKey()
    {
        return $this->key;
    }
}
