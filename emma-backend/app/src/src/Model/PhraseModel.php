<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class PhraseModel
{
    public ?int $id;

    public array $translations;

    public CategoryModel $category;

    public string $name;
}

