<?php

namespace App\Model;

use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class CountryModel
{
    public int $id;

    public string $name;

    /**
     * @SerializedName("languageKey")
     */
    public string $languageKey;
}
