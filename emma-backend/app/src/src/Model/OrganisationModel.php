<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class OrganisationModel
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var UserModel[]
     */
    public $users = [];
    /**
     * @var CategoryModel[]
     */
    public $categories = [];

    /**
     * @var PhraseModel[]
     */
    public $phrases = [];

    /**
     * @var CountryModel[]
     */
    public $countries = [];
}

