<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class CategoryModel
{
    public int $id;

    public string $category;

    public ?string $description;
}

