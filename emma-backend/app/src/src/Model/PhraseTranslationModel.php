<?php

namespace App\Model;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class PhraseTranslationModel
{
    public ?int $id;

    public string $translation;

    public string $languageKey;
}
