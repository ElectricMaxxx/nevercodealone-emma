<?php

namespace App\Model;

use App\Entity\User;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Maximilian Berghoff <maximilian.berghoff@gmx.de>
 */
class UserModel
{
    private ?int $id;
    private string $userName = '';
    private ?string $firstName;
    private ?string $lastName;
    private string $email = '';
    private ?int $organisationId;
    private ?string $organisationName;
    private string $role = 'ROLE_USER';
    private bool $isActive = true;

    private function __construct()
    {
    }
    public static function createEmpty(): self
    {
        return new self();
    }

    public static function fromEntity(User $user): self
    {
        $model = new self();
        $model->id = $user->getId();
        $model->userName = $user->getUsername();
        $model->firstName = $user->getFirstName();
        $model->lastName = $user->getLastName();
        $model->email =  $user->getEmail();
        $model->organisationId = $user->getOrganisation()->getId();
        $model->organisationName = $user->getOrganisation()->getName();
        $model->role = $user->getRoles()[0];

        return $model;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName(string $first_name): void
    {
        $this->firstName = $first_name;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int|null
     */
    public function getOrganisationId(): ?int
    {
        return $this->organisationId;
    }

    /**
     * @param int|null $organisationId
     */
    public function setOrganisationId(?int $organisationId): void
    {
        $this->organisationId = $organisationId;
    }

    /**
     * @return string|null
     */
    public function getOrganisationName(): ?string
    {
        return $this->organisationName;
    }

    /**
     * @param string|null $organisationName
     */
    public function setOrganisationName(?string $organisationName): void
    {
        $this->organisationName = $organisationName;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return bool
     */
    public function isIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }
}

