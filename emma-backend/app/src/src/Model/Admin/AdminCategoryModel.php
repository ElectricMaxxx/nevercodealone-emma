<?php

use App\Entity\Category;

class AdminCategoryModel
{

    private int $id;

    private string $category;

    private ?string $description;

    /**
     * @var AdminPhraseModel []
     */
    private array $phrases;

    private function __construct() {}

    public static function fromEntity(Category $category): self
    {
        $model = new self();
        $model->id = $category->getId();
        $model->description = $category->getDescription();
        $model->category = $category->getCategory();
        $model->phrases = array_map(static function($phrase) {
            return AdminPhraseModel::fromEntity($phrase);
        }, $category->getPhrases()->toArray());

        return $model;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return AdminPhraseModel[]
     */
    public function getPhrases(): array
    {
        return $this->phrases;
    }
}