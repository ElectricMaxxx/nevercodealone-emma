<?php

use App\Entity\Organisation;

class AdminOrganisationModel
{
    private ?int $id;
    private string $name;
    private array $countries;

    private function __construct() {}

    public static function fromEntity(Organisation $organisation): self
    {
        $model = new self();
        $model->id = $organisation->getId();
        $model->name = $organisation->getName();
        $model->countries = array_map(static function (\App\Entity\Country $country) {
            return AdminCountryModel::fromEntity($country);
        }, $organisation->getCountries()->toArray());

        return $model;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return AdminCountryModel[]
     */
    public function getCountries(): array
    {
        return $this->countries;
    }
}