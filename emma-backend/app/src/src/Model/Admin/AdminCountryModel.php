<?php

use App\Entity\Country;

class AdminCountryModel
{
    private ?int $id;

    private string $name;

    private string $languageKey;
    private function __construct() {}

    public static function fromEntity(Country $country): self
    {
        $model = new self();
        $model->id = $country->getId();
        $model->name = $country->getName();
        $model->languageKey = $country->getLanguageKey();

        return $model;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLanguageKey(): string
    {
        return $this->languageKey;
    }
}