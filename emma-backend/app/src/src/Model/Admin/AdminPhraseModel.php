<?php

use App\Entity\Phrase;
use App\Model\CategoryModel;
use App\Model\TranslationResponse;

class AdminPhraseModel
{
    public ?int $id;

    /**
     * @var TranslationResponse[]
     */
    public array $translations;

    public AdminOrganisationModel $organisation;

    public string $name;


    private function __construct() {}
    public static function fromEntity(Phrase $phrase): self
    {
        $model = new self();
        $model->id = $phrase->getId();
        $model->name = $phrase->getName();
        $model->organisation = AdminOrganisationModel::fromEntity($phrase->getOrganisation());
        $model->translations = array_map(static function(\App\Entity\Translation $translation) {
            return TranslationResponse::fromEntity($translation);
        }, $phrase->getTranslations()->toArray());


        return $model;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return TranslationResponse[]
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @return AdminOrganisationModel
     */
    public function getOrganisation(): AdminOrganisationModel
    {
        return $this->organisation;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}